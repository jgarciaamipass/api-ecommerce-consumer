package com.amipass.api.ecommerce_consumer.controller;

import com.amipass.api.ecommerce_consumer.model.ChannelShoppingEntity;
import com.amipass.api.ecommerce_consumer.services.ChannelShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/channel_shopping")
public class ChannelShoppingController {

    @Autowired
    private ChannelShoppingService channelShoppingService;

    @GetMapping("")
    public ResponseEntity<?> all() {
        return channelShoppingService.all();
    }

    @PostMapping("/store")
    public ResponseEntity<?> store(
            @Validated @RequestBody ChannelShoppingEntity channelShoppingEntity)
            throws ResponseStatusException {
        return channelShoppingService.store(channelShoppingEntity);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<?> show(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return channelShoppingService.show(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(
            @PathVariable(value = "id") Integer id,
            @Validated @RequestBody ChannelShoppingEntity channelShoppingEntity)
            throws ResponseStatusException {
        return channelShoppingService.update(channelShoppingEntity, id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return channelShoppingService.delete(id);
    }
}
