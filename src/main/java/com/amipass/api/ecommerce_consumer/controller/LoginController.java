package com.amipass.api.ecommerce_consumer.controller;

import com.amipass.api.ecommerce_consumer.model.AuthenticationRequest;
import com.amipass.api.ecommerce_consumer.model.dto.LoginResetPasswordDTO;
import com.amipass.api.ecommerce_consumer.model.UsersEntity;
import com.amipass.api.ecommerce_consumer.services.LoginService;
import com.amipass.api.ecommerce_consumer.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@Validated @RequestBody UsersEntity usersEntity)
            throws Exception {
        return loginService.register(usersEntity);
    }

    @PostMapping("")
    public ResponseEntity<?> login(@Validated @RequestBody AuthenticationRequest authenticationRequest)
            throws Exception {
        return loginService.login(authenticationRequest);
    }

    @PostMapping("/forgot_password")
    public ResponseEntity<?> forgotPassword(@Validated @RequestBody LoginResetPasswordDTO loginResetPasswordDTO)
            throws Exception {
        return loginService.forgotPassword(loginResetPasswordDTO.getEmail());
    }

    @GetMapping("/refresh_token")
    public ResponseEntity<?> refreshToken(HttpServletRequest request)
            throws Exception {
        return loginService.refreshToken(request);
    }

    @PostMapping("/activate_account/{token}")
    public ResponseEntity<?> activeAccount(@PathVariable(value = "token") String token)
            throws Exception {
        return loginService.active_account(token);
    }

    @PostMapping("/reset_password")
    public ResponseEntity<?> resetPassword(@Validated @RequestBody LoginResetPasswordDTO data)
            throws Exception {
        return loginService.resetPassword(data);
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout() {
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping("/email_exist")
    public ResponseEntity<?> emailExist(@Validated @RequestBody UsersEntity usersEntity)
            throws Exception {
        return userService.emailExist(usersEntity.getEmail());
    }

    @PostMapping("/document_exist")
    public ResponseEntity<?> documentExist(@Validated @RequestBody UsersEntity usersEntity)
            throws Exception {
        return userService.documentExist(usersEntity.getNumDocument());
    }

}
