package com.amipass.api.ecommerce_consumer.controller;

import com.amipass.api.ecommerce_consumer.model.PaymentTypeEntity;
import com.amipass.api.ecommerce_consumer.services.PaymentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/payment_type")
public class PaymentTypeController {

    @Autowired
    private PaymentTypeService paymentTypeService;

    @GetMapping("")
    public ResponseEntity<?> all() {
        return ResponseEntity.ok(paymentTypeService.all());
    }

    @PostMapping("/store")
    public ResponseEntity<?> store(@Validated @RequestBody PaymentTypeEntity paymentTypeEntity)
            throws ResponseStatusException {
        return paymentTypeService.store(paymentTypeEntity);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<?> show(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return paymentTypeService.show(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(
            @Validated @RequestBody PaymentTypeEntity paymentTypeEntity,
            @PathVariable(value = "id") Integer id) throws ResponseStatusException {
        return paymentTypeService.update(paymentTypeEntity, id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return paymentTypeService.delete(id);
    }
}
