package com.amipass.api.ecommerce_consumer.controller;

import com.amipass.api.ecommerce_consumer.model.UsersEntity;
import com.amipass.api.ecommerce_consumer.services.UserAddressService;
import com.amipass.api.ecommerce_consumer.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/api/v1/profile", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfileController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserAddressService userAddressService;

    @PostMapping("")
    public ResponseEntity<?> index(@Validated @RequestBody UsersEntity usersEntity)
            throws ResponseStatusException {
        return userService.findByEmail(usersEntity.getEmail());
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Integer id,
                              @Validated @RequestBody UsersEntity usersEntity)
            throws ResponseStatusException {
        return userService.update(usersEntity, id);
    }

    @GetMapping("/address/find/{id}")
    private ResponseEntity<?> findAddress(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return userAddressService.showByIdUser(id);
    }

    @GetMapping("/show/info_basic/{id}")
    public ResponseEntity<?> infoDelivery(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return userService.infoDelivery(id);
    }
}
