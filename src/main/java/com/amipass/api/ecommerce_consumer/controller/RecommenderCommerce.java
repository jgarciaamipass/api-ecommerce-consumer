package com.amipass.api.ecommerce_consumer.controller;

import com.amipass.api.ecommerce_consumer.model.RecommenderCommerceEntity;
import com.amipass.api.ecommerce_consumer.model.ShoppingCarEntity;
import com.amipass.api.ecommerce_consumer.services.RecommenderCommerceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/api/v1/recommender", produces = MediaType.APPLICATION_JSON_VALUE)
public class RecommenderCommerce {

    @Autowired
    private RecommenderCommerceService recommenderCommerceService;

    @PostMapping("/store")
    public ResponseEntity<?> store(@Validated @RequestBody RecommenderCommerceEntity recommenderCommerceEntity)
            throws ResponseStatusException {
        return recommenderCommerceService.store(recommenderCommerceEntity);
    }
}
