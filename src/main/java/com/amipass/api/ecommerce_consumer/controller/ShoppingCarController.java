package com.amipass.api.ecommerce_consumer.controller;

import com.amipass.api.ecommerce_consumer.model.ShoppingCarEntity;
import com.amipass.api.ecommerce_consumer.services.ShoppingCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/shopping")
public class ShoppingCarController {

    @Autowired
    private ShoppingCarService shoppingCarService;

    @GetMapping(value = "/my_orders/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> all(@PathVariable(value = "id") Integer id) throws Exception {
        return shoppingCarService.all(id);
    }

    @PostMapping("/store")
    public ResponseEntity<?> store(@Validated @RequestBody ShoppingCarEntity shoppingCarEntity)
            throws ResponseStatusException {
        return shoppingCarService.store(shoppingCarEntity);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<?> show(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return shoppingCarService.show(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(
            @Validated @RequestBody ShoppingCarEntity shoppingCarEntity,
            @PathVariable(value = "id") Integer id) throws ResponseStatusException {
        return shoppingCarService.update(shoppingCarEntity, id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return shoppingCarService.delete(id);
    }
}
