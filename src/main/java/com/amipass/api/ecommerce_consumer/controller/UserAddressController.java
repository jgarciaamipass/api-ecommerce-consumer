package com.amipass.api.ecommerce_consumer.controller;

import com.amipass.api.ecommerce_consumer.model.UserAddressEntity;
import com.amipass.api.ecommerce_consumer.services.UserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/profile/address")
public class UserAddressController {

    @Autowired
    UserAddressService userAddressService;

    @GetMapping("")
    public ResponseEntity<?> all() {
        return userAddressService.all();
    }

    @PostMapping("/store")
    public ResponseEntity<?> create(@Validated @RequestBody UserAddressEntity userAddressEntity) throws ResponseStatusException {
        return userAddressService.store(userAddressEntity);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<?> show(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return userAddressService.show(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(
            @Validated @RequestBody UserAddressEntity userAddressEntity,
            @PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return userAddressService.update(userAddressEntity, id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return userAddressService.delete(id);
    }
}
