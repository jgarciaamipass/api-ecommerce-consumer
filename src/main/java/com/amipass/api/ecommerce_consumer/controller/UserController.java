package com.amipass.api.ecommerce_consumer.controller;

import com.amipass.api.ecommerce_consumer.model.UsersEntity;
import com.amipass.api.ecommerce_consumer.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/system/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return userService.all();
    }

    @PostMapping("/store")
    public ResponseEntity<?> create(@Validated @RequestBody UsersEntity usersEntity)
            throws ResponseStatusException {
        return userService.store(usersEntity);
    }

    @GetMapping("/show/email/{email}")
    public ResponseEntity<?> findByEmail(@PathVariable(value = "email") String email)
            throws ResponseStatusException {
        return ResponseEntity.ok(userService.findByEmail(email));
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<?> show(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return ResponseEntity.ok(userService.show(id));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Integer id,
                                   @Validated @RequestBody UsersEntity users)
            throws ResponseStatusException {
        return ResponseEntity.ok(userService.update(users, id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id)
            throws ResponseStatusException {
        return userService.delete(id);
    }
}
