package com.amipass.api.ecommerce_consumer.controller.amipass;

import com.amipass.api.ecommerce_consumer.model.dto.amipass.AccountAmipassDTO;
import com.amipass.api.ecommerce_consumer.model.dto.amipass.AccountAmipassLoginDTO;
import com.amipass.api.ecommerce_consumer.services.amipass.AccountAmipassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/api/v1/profile/account/amipass", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountAmipassController {

    @Autowired
    private AccountAmipassService accountAmipassService;

    @PostMapping("")
    public Object showFromAmipass(@Validated @RequestBody AccountAmipassLoginDTO accountAmipassLoginDTO)
            throws Exception {
        return accountAmipassService.showFromAmipass(accountAmipassLoginDTO);
    }

    @PostMapping("/store/{id}")
    private Object store(@Validated @RequestBody List<AccountAmipassDTO> accountAmipassDTO, @PathVariable(value = "id") Integer id)
            throws Exception {
        return accountAmipassService.store(accountAmipassDTO, id);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<?> show(@PathVariable(value = "id") Integer id)
            throws Exception {
        return accountAmipassService.show(id);
    }
}
