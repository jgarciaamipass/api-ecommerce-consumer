package com.amipass.api.ecommerce_consumer.controller.finance;

import com.amipass.api.ecommerce_consumer.model.dto.finance.PayAmipassDTO;
import com.amipass.api.ecommerce_consumer.services.finance.PaymentAmipassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/api/v1/pay/amipass", produces = MediaType.APPLICATION_JSON_VALUE)
public class PayFinanceAmipassController {

    @Autowired
    private PaymentAmipassService paymentAmipassService;

    @PostMapping("")
    private ResponseEntity<?> pay(@Validated @RequestBody PayAmipassDTO payAmipassDTO)
            throws Exception {
        return paymentAmipassService.pay(payAmipassDTO);
    }
}
