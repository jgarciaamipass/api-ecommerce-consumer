package com.amipass.api.ecommerce_consumer.controller.finance;

import com.amipass.api.ecommerce_consumer.model.dto.finance.AccountTransbankDTO;
import com.amipass.api.ecommerce_consumer.model.dto.finance.PayTransbankDTO;
import com.amipass.api.ecommerce_consumer.services.finance.PaymentTransbankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value ="/api/v1/pay/transbank", produces = MediaType.APPLICATION_JSON_VALUE)
public class PayFinanceTransbankController {

    @Autowired
    PaymentTransbankService paymentTransbankService;

    @PostMapping("")
    private ResponseEntity<?> pay(@Validated @RequestBody PayTransbankDTO payTransbankDTO)
            throws Exception {
        return paymentTransbankService.pay(payTransbankDTO);
    }

    @PostMapping("/card")
    private ResponseEntity<?> cards(@Validated @RequestBody AccountTransbankDTO accountTransbankDTO)
            throws Exception {
        return paymentTransbankService.cards(accountTransbankDTO);
    }

    @PostMapping("/card/subscribe")
    private ResponseEntity<?> subscribe(@Validated @RequestBody AccountTransbankDTO accountTransbankDTO )
            throws Exception {
        return paymentTransbankService.subscribe(accountTransbankDTO);
    }

    @DeleteMapping("/card/unsubscribe")
    private ResponseEntity<?> unsubscribe(@Validated @RequestBody AccountTransbankDTO accountTransbankDTO)
            throws Exception {
        return paymentTransbankService.unsubscribe(accountTransbankDTO);
    }
}
