package com.amipass.api.ecommerce_consumer.controller.mac;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/api/v1/commerce/classification", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommerceClassificationController {
    final String uri = "https://servicespay.amipass.cl/apimac/api/v1/mac";

    @GetMapping("")
    public ResponseEntity<?> show() throws Exception {
        try {
            RestTemplate restTemplate = new RestTemplate();
            URI uri = new URI(String.format("%s/commerces/clasifications", this.uri));
            return ResponseEntity.ok(restTemplate.getForEntity(uri, String.class));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
