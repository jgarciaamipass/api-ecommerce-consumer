package com.amipass.api.ecommerce_consumer.controller.mac;

import com.amipass.api.ecommerce_consumer.services.mac.CommerceService;
import com.amipass.api.ecommerce_consumer.services.mac.FinderParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/api/v1/commerce", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommerceController {

    @Autowired
    private CommerceService commerceService;

    @PostMapping("")
    private ResponseEntity<?> all(@Validated @RequestBody FinderParams params, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC) throws Exception {
        try {
            commerceService.tokenMAC = jwtMAC;
            return commerceService.all(params);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    @GetMapping("/show/{code}")
    private ResponseEntity<?> show(@PathVariable(value = "code") String code, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        try {
            commerceService.tokenMAC = jwtMAC;
            return commerceService.show(code);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    @GetMapping("/show/name/{name}")
    private ResponseEntity<?> showByName(@PathVariable(value = "name") String name, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        try {
            commerceService.tokenMAC = jwtMAC;
            return commerceService.showByName(name);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    @GetMapping("/category/{category}")
    private ResponseEntity<?> category(@PathVariable(value = "category") String category, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        try {
            commerceService.tokenMAC = jwtMAC;
            return commerceService.category(category);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    @GetMapping("/product/{code}")
    private ResponseEntity<?> products(@PathVariable(value = "code") String code, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        try {
            commerceService.tokenMAC = jwtMAC;
            return commerceService.products(code);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    @PostMapping("/product/validate/{commerce}")
    private ResponseEntity<?> validateProducts(@PathVariable(value = "commerce") String commerce, @Validated @RequestBody Object products, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        try {
            commerceService.tokenMAC = jwtMAC;
            return commerceService.validateProducts(commerce, products);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    @GetMapping("/state/{code}")
    private ResponseEntity<?> isOpen(@PathVariable(value = "code") String code, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        try {
            commerceService.tokenMAC = jwtMAC;
            return commerceService.isOpen(code);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }
}
