package com.amipass.api.ecommerce_consumer.controller.mac;

import com.amipass.api.ecommerce_consumer.model.dto.mac.OrderStatusDTO;
import com.amipass.api.ecommerce_consumer.services.mac.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/api/v1/shopping/order", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping(value = "/store/{id}")
    public Object store(@PathVariable(value = "id") Integer id, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        orderService.tokenMAC = jwtMAC;
        return orderService.store(id);
    }

    @GetMapping(value = "/show/{id}")
    public ResponseEntity<?> show(@PathVariable(value = "id") Integer id, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        orderService.tokenMAC = jwtMAC;
        return orderService.show(id);
    }

    @PostMapping("/status/{id}")
    public Object setStatusOrder(@PathVariable(value = "id") Integer id, @Validated @RequestBody OrderStatusDTO orderStatusDTO, @RequestHeader(required = false, value = "jwtMAC") String jwtMAC)
            throws Exception {
        orderService.tokenMAC = jwtMAC;
        return orderService.changeStatusOrder(id, orderStatusDTO);
    }
}
