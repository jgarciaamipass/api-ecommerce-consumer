package com.amipass.api.ecommerce_consumer.model;

public class AuthenticationRequest {

    private String email;
    private String password;
    private Boolean remember;

    public AuthenticationRequest() {}

    public AuthenticationRequest(String email, String password, Boolean remember) {
        this.email = email;
        this.password = password;
        this.remember = remember;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getRemember() {
        return remember != null && remember;
    }

    public void setRemember(Boolean remember) {
        this.remember = remember;
    }
}
