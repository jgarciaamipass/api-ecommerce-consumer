package com.amipass.api.ecommerce_consumer.model;

public class AuthenticationResponse {

    private final String jwt;
    private final int iduser;
    private final String fullname;

    public AuthenticationResponse(String jwt, int iduser, String fullname) {
        this.jwt = jwt;
        this.iduser = iduser;
        this.fullname = fullname;
    }

    public String getJwt() {
        return jwt;
    }

    public int getIduser() {
        return iduser;
    }

    public String getFullname() {
        return fullname;
    }
}
