package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "channel_shopping", schema = "dbo", catalog = "Amipass_Amimarket")
public class ChannelShoppingEntity {
    private int id;
    private String name;
    private Collection<ShoppingCarEntity> shoppingCarsByChannelShoppingId;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 10)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChannelShoppingEntity that = (ChannelShoppingEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "channelShoppingByIdchannelShopping")
//    public Collection<ShoppingCarEntity> getShoppingCarsByIdchannelShopping() {
//        return shoppingCarsByIdchannelShopping;
//    }
//
//    public void setShoppingCarsByIdchannelShopping(Collection<ShoppingCarEntity> shoppingCarsByIdchannelShopping) {
//        this.shoppingCarsByIdchannelShopping = shoppingCarsByIdchannelShopping;
//    }
}
