package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "payment_type", schema = "dbo", catalog = "Amipass_Amimarket")
public class PaymentTypeEntity {
    private int id;
    private String name;
    private Collection<ShoppingCarEntity> shoppingCarsByPaymentTypeId;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getPaymentTypeId() {
        return id;
    }

    public void setPaymentTypeId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentTypeEntity that = (PaymentTypeEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "paymentTypeByIdpaymentType")
//    public Collection<ShoppingCarEntity> getShoppingCarsByIdpaymentType() {
//        return shoppingCarsByIdpaymentType;
//    }
//
//    public void setShoppingCarsByIdpaymentType(Collection<ShoppingCarEntity> shoppingCarsByIdpaymentType) {
//        this.shoppingCarsByIdpaymentType = shoppingCarsByIdpaymentType;
//    }
}
