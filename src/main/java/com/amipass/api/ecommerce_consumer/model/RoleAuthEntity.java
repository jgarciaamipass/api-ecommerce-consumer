package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "role_auth", schema = "dbo", catalog = "Amipass_Amimarket")
public class RoleAuthEntity {
    private int id;
    private String name;
    private String description;
    private int priority;
    private Collection<UserRoleAuthEntity> usersRoleAuthsByIdroleAuth;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = -1)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "priority", nullable = false)
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleAuthEntity that = (RoleAuthEntity) o;

        if (id != that.id) return false;
        if (priority != that.priority) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + priority;
        return result;
    }

    @OneToMany(mappedBy = "roleAuthByIdroleAuth")
    public Collection<UserRoleAuthEntity> getUsersRoleAuthsByIdroleAuth() {
        return usersRoleAuthsByIdroleAuth;
    }

    public void setUsersRoleAuthsByIdroleAuth(Collection<UserRoleAuthEntity> usersRoleAuthsByIdroleAuth) {
        this.usersRoleAuthsByIdroleAuth = usersRoleAuthsByIdroleAuth;
    }
}
