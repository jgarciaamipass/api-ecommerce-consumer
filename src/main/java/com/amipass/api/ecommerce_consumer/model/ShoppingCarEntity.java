package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Entity
@Table(name = "shopping_car", schema = "dbo", catalog = "Amipass_Amimarket")
public class ShoppingCarEntity {
    private int id;
    private int userId;
    private int commerce;
    private String store;
    private int channelShoppingId;
    private int status;
    private double totalAmount;
    private double discountAmount = 0;
    private double deliveryAmount = 0;
    private int userAddressId;
    private String comment;
    private int paymentTypeId;
    private boolean delivery;
    private int orderIdMac;
    private int tryPay;
    private String urlInvoice;
    private String phone;
    private String fullName;
    private Date createdAt;
    private Date updatedAt;
    private UsersEntity usersByIdusers;
    private ChannelShoppingEntity channelShoppingByChannelShoppingId;
    private UserAddressEntity usersAddressByIdusersAddress;
    private PaymentTypeEntity paymentTypeByIdpaymentType;
    private Collection<ShoppingCarItemEntity> shoppingCarItemsByShoppingCarId;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "commerce", nullable = false)
    public int getCommerce() {
        return commerce;
    }

    public void setCommerce(int commerce) {
        this.commerce = commerce;
    }

    @Basic
    @Column(name = "store", nullable = false)
    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    @Basic
    @Column(name = "channel_shopping_id", nullable = false)
    public int getChannelShoppingId() {
        return channelShoppingId;
    }

    public void setChannelShoppingId(int channelShoppingId) {
        this.channelShoppingId = channelShoppingId;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "total_amount", nullable = false, precision = 2)
    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "delivery_amount", nullable = false, precision = 2)
    public double getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(double deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    @Basic
    @Column(name = "discount_amount", nullable = false, precision = 2)
    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Basic
    @Column(name = "user_address_id", nullable = true)
    public int getUserAddressId() {
        return userAddressId;
    }

    public void setUserAddressId(int userAddressId) {
        this.userAddressId = userAddressId;
    }

    @Basic
    @Column(name = "comment", nullable = true, length = -1)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "payment_type_id", nullable = false)
    public int getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(int paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    @Basic
    @Column(name = "delivery", nullable = false)
    public boolean getDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    @Basic
    @Column(name = "order_id_mac", nullable = false)
    public int getOrderIdMac() {
        return orderIdMac;
    }

    public void setOrderIdMac(int orderIdMac) {
        this.orderIdMac = orderIdMac;
    }

    @Basic
    @Column(name = "try_pay", nullable = false)
    public int getTryPay() {
        return tryPay;
    }

    public void setTryPay(int tryPay) {
        this.tryPay = tryPay;
    }

    @Basic
    @Column(name = "url_invoice", nullable = true)
    public String getUrlInvoice() {
        return urlInvoice;
    }

    public void setUrlInvoice(String urlInvoice) {
        this.urlInvoice = urlInvoice;
    }

    @Basic
    @Column(name = "phone", nullable = true)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "full_name", nullable = true)
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "created_at", nullable = false)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at", nullable = false)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShoppingCarEntity that = (ShoppingCarEntity) o;

        if (id != that.id) return false;
        if (userId != that.userId) return false;
        if (commerce != that.commerce) return false;
        if (store != that.store) return false;
        if (channelShoppingId != that.channelShoppingId) return false;
        if (status != that.status) return false;
        if (userAddressId != that.userAddressId) return false;
        if (paymentTypeId != that.paymentTypeId) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
        if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + commerce;
        result = 31 * result + channelShoppingId;
        result = 31 * result + (int) status;
        result = 31 * result + userAddressId;
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + paymentTypeId;
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
        return result;
    }

//    @ManyToOne
//    @JoinColumn(name = "userId", referencedColumnName = "userId", nullable = false, table = "shopping_car", insertable = false, updatable = false)
//    public UsersEntity getUsersByIdusers() {
//        return usersByIdusers;
//    }
//
//    public void setUsersByIdusers(UsersEntity usersByIdusers) {
//        this.usersByIdusers = usersByIdusers;
//    }

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "channel_shopping_id", referencedColumnName = "id", nullable = false, table = "shopping_car", insertable = false, updatable = false)
    public ChannelShoppingEntity getChannelShoppingByIdchannelShopping() {
        return channelShoppingByChannelShoppingId;
    }

    public void setChannelShoppingByIdchannelShopping(ChannelShoppingEntity channelShoppingByChannelShoppingId) {
        this.channelShoppingByChannelShoppingId = channelShoppingByChannelShoppingId;
    }

//    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "user_addressId", referencedColumnName = "user_addressId", nullable = false, insertable = false, updatable = false)
//    public UsersAddressEntity getUsersAddressByIdusersAddress() {
//        return usersAddressByIdusersAddress;
//    }
//
//    public void setUsersAddressByIdusersAddress(UsersAddressEntity usersAddressByIdusersAddress) {
//        this.usersAddressByIdusersAddress = usersAddressByIdusersAddress;
//    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shopping_car_id")
    public Collection<ShoppingCarItemEntity> getShoppingCarItemsByShoppingCarId() {
        return shoppingCarItemsByShoppingCarId;
    }

    public void setShoppingCarItemsByShoppingCarId(Collection<ShoppingCarItemEntity> shoppingCarItemsByShoppingCarId) {
        this.shoppingCarItemsByShoppingCarId = shoppingCarItemsByShoppingCarId;
    }
}
