package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;

@Entity
@Table(name = "shopping_car_item", schema = "dbo", catalog = "Amipass_Amimarket")
public class ShoppingCarItemEntity {
    private int id;
    private int shoppingCarId;
    private int productIdMac;
    private String name;
    private String comment;
    private int quantity;
    private double unitPrice;
    private double discountAmount;
    private double total;
    private ShoppingCarEntity shoppingCarById;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "shopping_car_id", nullable = false)
    public int getShoppingCarId() {
        return shoppingCarId;
    }

    public void setShoppingCarId(int shoppingCarId) {
        this.shoppingCarId = shoppingCarId;
    }

    @Basic
    @Column(name = "product_id_mac", nullable = false)
    public int getProductIdMac() {
        return productIdMac;
    }

    public void setProductIdMac(int productIdMac) {
        this.productIdMac = productIdMac;
    }

    @Basic
    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "unit_price", nullable = false, precision = 2)
    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "discount_amount", nullable = false, precision = 2)
    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    @Basic
    @Column(name = "total", nullable = false, precision = 2)
    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Basic
    @Column(name = "comment", nullable = true, precision = 2)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShoppingCarItemEntity that = (ShoppingCarItemEntity) o;

        if (id != that.id) return false;
        if (shoppingCarId != that.shoppingCarId) return false;
        if (productIdMac != that.productIdMac) return false;
        if (name != that.name) return false;
        if (quantity != that.quantity) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + shoppingCarId;
        result = 31 * result + productIdMac;
        result = 31 * result + quantity;
        return result;
    }

//    @ManyToOne
//    @JoinColumn(name = "idshopping_car", referencedColumnName = "idshopping_car", nullable = false, table = "shopping_car_item", insertable = false, updatable = false)
//    public ShoppingCarEntity getShoppingCarByIdshoppingCar() {
//        return shoppingCarByIdshoppingCar;
//    }
//
//    public void setShoppingCarByIdshoppingCar(ShoppingCarEntity shoppingCarByIdshoppingCar) {
//        this.shoppingCarByIdshoppingCar = shoppingCarByIdshoppingCar;
//    }
}
