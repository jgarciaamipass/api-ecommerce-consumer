package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Entity
@Table(name = "user_account_amipass", schema = "dbo", catalog = "Amipass_Amimarket")
public class UserAccountAmipassEntity {
    private int id;
    private int userId;
    private String employee;
    private String employer;
    private Date createdAt;
    private Date updatedAt;
    private String employerName;
    private String employeeName;
    private String cardCode;
    private Collection<UsersEntity> usersByUsersAccountAmipassId;
    private UsersEntity usersByIdusers;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "employee", nullable = false, length = 20)
    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    @Basic
    @Column(name = "employer", nullable = false, length = 20)
    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    @Basic
    @Column(name = "employer_name", nullable = true, length = 255)
    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    @Basic
    @Column(name = "employee_name", nullable = true, length = 255)
    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Basic
    @Column(name = "card_code", nullable = true, length = 15)
    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    @Basic
    @Column(name = "created_at", nullable = false)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at", nullable = false)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAccountAmipassEntity that = (UserAccountAmipassEntity) o;

        if (id != that.id) return false;
        if (userId != that.userId) return false;
        if (employee != null ? !employee.equals(that.employee) : that.employee != null) return false;
        if (employer != null ? !employer.equals(that.employer) : that.employer != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
        if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + (employee != null ? employee.hashCode() : 0);
        result = 31 * result + (employer != null ? employer.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "usersAccountAmipassByIdusersAccountAmipass")
//    public Collection<UsersEntity> getUsersByIdusersAccountAmipass() {
//        return usersByIdusersAccountAmipass;
//    }
//
//    public void setUsersByIdusersAccountAmipass(Collection<UsersEntity> usersByIdusersAccountAmipass) {
//        this.usersByIdusersAccountAmipass = usersByIdusersAccountAmipass;
//    }

//    @ManyToOne
//    @JoinColumn(name = "userId", referencedColumnName = "userId", nullable = false, table = "users_account_amipass", insertable = false, updatable = false)
//    public UsersEntity getUsersByIdusers() {
//        return usersByIdusers;
//    }
//
//    public void setUsersByIdusers(UsersEntity usersByIdusers) {
//        this.usersByIdusers = usersByIdusers;
//    }
}
