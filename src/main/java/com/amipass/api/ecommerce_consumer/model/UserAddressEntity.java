package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Entity
@Table(name = "user_address", schema = "dbo", catalog = "Amipass_Amimarket")
public class UserAddressEntity {
    private int id;
    private int userId;
    private String address;
    private String latitude;
    private String longitude;
    private Integer streetNumber;
    private String route;
    private String locality;
    private String areaLevel3;
    private String areaLevel2;
    private String areaLevel1;
    private String country;
    private String place;
    private String label;
    private Date createdAt;
    private Date updatedAt;
    private Collection<ShoppingCarEntity> shoppingCarsByIdusersAddress;
    private Collection<UsersEntity> usersByIdusersAddress;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "address", nullable = true, length = -1)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "latitude", nullable = true, precision = 0)
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "longitude", nullable = true, precision = 0)
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "street_number", nullable = true)
    public Integer getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(Integer streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Basic
    @Column(name = "route", nullable = true, length = 120)
    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    @Basic
    @Column(name = "locality", nullable = true, length = 120)
    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    @Basic
    @Column(name = "area_level_3", nullable = true, length = 120)
    public String getAreaLevel3() {
        return areaLevel3;
    }

    public void setAreaLevel3(String areaLevel3) {
        this.areaLevel3 = areaLevel3;
    }

    @Basic
    @Column(name = "area_level_2", nullable = true, length = 120)
    public String getAreaLevel2() {
        return areaLevel2;
    }

    public void setAreaLevel2(String areaLevel2) {
        this.areaLevel2 = areaLevel2;
    }

    @Basic
    @Column(name = "area_level_1", nullable = true, length = 120)
    public String getAreaLevel1() {
        return areaLevel1;
    }

    public void setAreaLevel1(String areaLevel1) {
        this.areaLevel1 = areaLevel1;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 120)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "place", nullable = true, length = 60)
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
    @Basic
    @Column(name = "label", nullable = true, length = 60)
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Basic
    @Column(name = "created_at", nullable = true)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at", nullable = true)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAddressEntity that = (UserAddressEntity) o;

        if (id != that.id) return false;
        if (userId != that.userId) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (streetNumber != null ? !streetNumber.equals(that.streetNumber) : that.streetNumber != null) return false;
        if (route != null ? !route.equals(that.route) : that.route != null) return false;
        if (locality != null ? !locality.equals(that.locality) : that.locality != null) return false;
        if (areaLevel3 != null ? !areaLevel3.equals(that.areaLevel3) : that.areaLevel3 != null) return false;
        if (areaLevel2 != null ? !areaLevel2.equals(that.areaLevel2) : that.areaLevel2 != null) return false;
        if (areaLevel1 != null ? !areaLevel1.equals(that.areaLevel1) : that.areaLevel1 != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
        if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (streetNumber != null ? streetNumber.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        result = 31 * result + (locality != null ? locality.hashCode() : 0);
        result = 31 * result + (areaLevel3 != null ? areaLevel3.hashCode() : 0);
        result = 31 * result + (areaLevel2 != null ? areaLevel2.hashCode() : 0);
        result = 31 * result + (areaLevel1 != null ? areaLevel1.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "usersAddressByIdusersAddress")
//    public Collection<ShoppingCarEntity> getShoppingCarsByIdusersAddress() {
//        return shoppingCarsByIdusersAddress;
//    }
//
//    public void setShoppingCarsByIdusersAddress(Collection<ShoppingCarEntity> shoppingCarsByIdusersAddress) {
//        this.shoppingCarsByIdusersAddress = shoppingCarsByIdusersAddress;
//    }

//    @OneToMany(mappedBy = "usersAddressByIdusersAddress")
//    public Collection<UsersEntity> getUsersByIdusersAddress() {
//        return usersByIdusersAddress;
//    }
//
//    public void setUsersByIdusersAddress(Collection<UsersEntity> usersByIdusersAddress) {
//        this.usersByIdusersAddress = usersByIdusersAddress;
//    }
}
