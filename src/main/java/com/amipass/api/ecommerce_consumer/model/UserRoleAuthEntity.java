package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;

@Entity
@Table(name = "user_role_auth", schema = "dbo", catalog = "Amipass_Amimarket")
public class UserRoleAuthEntity {
    private int id;
    private int userId;
    private int roleAuthId;
    private UsersEntity usersByIdusers;
    private RoleAuthEntity roleAuthByIdroleAuth;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getIdusersRoleAuth() {
        return id;
    }

    public void setIdusersRoleAuth(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public int getIdusers() {
        return userId;
    }

    public void setIdusers(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "role_auth_id", nullable = false)
    public int getIdroleAuth() {
        return roleAuthId;
    }

    public void setIdroleAuth(int roleAuthId) {
        this.roleAuthId = roleAuthId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRoleAuthEntity that = (UserRoleAuthEntity) o;

        if (id != that.id) return false;
        if (userId != that.userId) return false;
        if (roleAuthId != that.roleAuthId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + roleAuthId;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, table = "user_role_auth", insertable = false, updatable = false)
    public UsersEntity getUsersByIdusers() {
        return usersByIdusers;
    }

    public void setUsersByIdusers(UsersEntity usersByIdusers) {
        this.usersByIdusers = usersByIdusers;
    }

    @ManyToOne
    @JoinColumn(name = "role_auth_id", referencedColumnName = "id", nullable = false, table = "user_role_auth", insertable = false, updatable = false)
    public RoleAuthEntity getRoleAuthByIdroleAuth() {
        return roleAuthByIdroleAuth;
    }

    public void setRoleAuthByIdroleAuth(RoleAuthEntity roleAuthByIdroleAuth) {
        this.roleAuthByIdroleAuth = roleAuthByIdroleAuth;
    }
}
