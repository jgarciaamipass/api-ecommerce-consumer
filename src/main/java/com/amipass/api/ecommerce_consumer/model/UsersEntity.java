package com.amipass.api.ecommerce_consumer.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Collection;

@Entity
@Table(name = "users", schema = "dbo", catalog = "Amipass_Amimarket")
public class UsersEntity {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String rememberToken;
    private boolean active;
    private String numDocument;
    private Integer idusersAddress;
    private String phone;
    private Integer countOfDeclined;
    private Integer userAccountAmipassId;
    private Date birthday;
    private Date createdAt;
    private Date updatedAt;
    private Collection<ShoppingCarEntity> shoppingCarsByIdusers;
    private UserAddressEntity usersAddressByUsersAddressId;
    private UserAccountAmipassEntity usersAccountAmipassByIdusersAccountAmipass;
    private Collection<UserAccountAmipassEntity> usersAccountAmipassesByIdusers;
    private Collection<UserRoleAuthEntity> usersRoleAuthsByUsersId;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 60)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 60)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "email", nullable = false, length = 120)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "password", nullable = false, length = -1)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "created_at", nullable = false)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at", nullable = false)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Basic
    @Column(name = "birthday", nullable = true)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "num_document", nullable = false)
    public String getNumDocument() {
        return numDocument;
    }

    public void setNumDocument(String numDocument) {
        this.numDocument = numDocument;
    }

    @Basic
    @Column(name = "user_address_id", nullable = true)
    public Integer getIdusersAddress() {
        return idusersAddress;
    }

    public void setIdusersAddress(Integer idusersAddress) {
        this.idusersAddress = idusersAddress;
    }

    @Basic
    @Column(name = "phone", nullable = true, length = 120)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "user_account_amipass_id", nullable = true)
    public Integer getIdusersAccountAmipass() {
        return userAccountAmipassId;
    }

    public void setIdusersAccountAmipass(Integer userAccountAmipassId) {
        this.userAccountAmipassId = userAccountAmipassId;
    }

    @Basic
    @Column(name = "remember_token", nullable = false)
    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    @Basic
    @Column(name = "count_of_declined", nullable = false)
    public Integer getCountOfDeclined() {
        return countOfDeclined;
    }

    public void setCountOfDeclined(Integer countOfDeclined) {
        this.countOfDeclined = countOfDeclined;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (id != that.id) return false;
        if (active != that.active) return false;
        if (numDocument != that.numDocument) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
        if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
        if (idusersAddress != null ? !idusersAddress.equals(that.idusersAddress) : that.idusersAddress != null)
            return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (userAccountAmipassId != null ? !userAccountAmipassId.equals(that.userAccountAmipassId) : that.userAccountAmipassId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        result = 31 * result + (idusersAddress != null ? idusersAddress.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (userAccountAmipassId != null ? userAccountAmipassId.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "usersByIdusers")
//    public Collection<ShoppingCarEntity> getShoppingCarsByIdusers() {
//        return shoppingCarsByIdusers;
//    }
//
//    public void setShoppingCarsByIdusers(Collection<ShoppingCarEntity> shoppingCarsByIdusers) {
//        this.shoppingCarsByIdusers = shoppingCarsByIdusers;
//    }

    @ManyToOne
    @JoinColumn(name = "user_address_id", referencedColumnName = "id", table = "users", insertable = false, updatable = false)
    public UserAddressEntity getUsersAddressByIdusersAddress() {
        return usersAddressByUsersAddressId;
    }

    public void setUsersAddressByIdusersAddress(UserAddressEntity usersAddressByUsersAddressId) {
        this.usersAddressByUsersAddressId = usersAddressByUsersAddressId;
    }

//    @ManyToOne
//    @JoinColumn(name = "idusers_account_amipass", referencedColumnName = "idusers_account_amipass", table = "users", insertable = false, updatable = false)
//    public UserAccountAmipassEntity getUsersAccountAmipassByIdusersAccountAmipass() {
//        return usersAccountAmipassByIdusersAccountAmipass;
//    }
//
//    public void setUsersAccountAmipassByIdusersAccountAmipass(UserAccountAmipassEntity usersAccountAmipassByIdusersAccountAmipass) {
//        this.usersAccountAmipassByIdusersAccountAmipass = usersAccountAmipassByIdusersAccountAmipass;
//    }

//    @OneToMany(mappedBy = "usersByIdusers")
//    public Collection<UserAccountAmipassEntity> getUsersAccountAmipassesByIdusers() {
//        return usersAccountAmipassesByIdusers;
//    }
//
//    public void setUsersAccountAmipassesByIdusers(Collection<UserAccountAmipassEntity> usersAccountAmipassesByIdusers) {
//        this.usersAccountAmipassesByIdusers = usersAccountAmipassesByIdusers;
//    }

    @OneToMany(mappedBy = "usersByIdusers")
    public Collection<UserRoleAuthEntity> getUsersRoleAuthsByIdusers() {
        return usersRoleAuthsByUsersId;
    }

    public void setUsersRoleAuthsByIdusers(Collection<UserRoleAuthEntity> usersRoleAuthsByUsersId) {
        this.usersRoleAuthsByUsersId = usersRoleAuthsByUsersId;
    }
}
