package com.amipass.api.ecommerce_consumer.model.dto.finance;

import java.io.Serializable;

public class AccountTransbankDTO implements Serializable {
    private int id;
    private int status;
    private int responseCode;
    private String username;
    private String email;
    private String creditCardType;
    private String lastFourCardDigits;
    private String responseUrl;

    public String getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getLastFourCardDigits() {
        return lastFourCardDigits;
    }

    public void setLastFourCardDigits(String lastFourCardDigits) {
        this.lastFourCardDigits = lastFourCardDigits;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseUrl() {
        return responseUrl;
    }

    public void setResponseUrl(String responseUrl) {
        this.responseUrl = responseUrl;
    }
}

//{
//    "response_code": 0,
//    "id": 4,
//    "status": 0,
//    "username": "joise",
//    "email": "j.garcia-ext@amipass.com",
//    "response_url": null,
//    "url_webpay": null,
//    "tbk_token": null,
//    "tbk_user": null,
//    "authorization_code": null,
//    "credit_card_type": "Visa",
//    "last_four_card_digits": "XXXXXXXXXXXX6623"
//}
