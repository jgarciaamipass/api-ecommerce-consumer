package com.amipass.api.ecommerce_consumer.model.dto.finance;

import java.io.Serializable;

public class PayAmipassDTO implements Serializable {
    private String sEmpleado;
    private String sEmpresa;
    private String sClave;
    private String sEstablecimiento;
    private String sLocal;
    private String nMonto;
    private String buyOrder;

    public String getsEmpleado() {
        return sEmpleado;
    }

    public void setsEmpleado(String sEmpleado) {
        this.sEmpleado = sEmpleado;
    }

    public String getsEmpresa() {
        return sEmpresa;
    }

    public void setsEmpresa(String sEmpresa) {
        this.sEmpresa = sEmpresa;
    }

    public String getsClave() {
        return sClave;
    }

    public void setsClave(String sClave) {
        this.sClave = sClave;
    }

    public String getsEstablecimiento() {
        return sEstablecimiento;
    }

    public void setsEstablecimiento(String sEstablecimiento) {
        this.sEstablecimiento = sEstablecimiento;
    }

    public String getsLocal() {
        return sLocal;
    }

    public void setsLocal(String sLocal) {
        this.sLocal = sLocal;
    }

    public String getnMonto() {
        return nMonto;
    }

    public void setnMonto(String nMonto) {
        this.nMonto = nMonto;
    }

    public String getBuyOrder() {
        return buyOrder;
    }

    public void setBuyOrder(String buyOrder) {
        this.buyOrder = buyOrder;
    }
}
