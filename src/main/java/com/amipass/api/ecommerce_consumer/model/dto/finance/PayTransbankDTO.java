package com.amipass.api.ecommerce_consumer.model.dto.finance;

import java.io.Serializable;

public class PayTransbankDTO implements Serializable {
    private String username;
    private int card;
    private double amount;
    private int shopping;
    private String commerce;
    private String store;
    private int installments = 1;
    private boolean eticket = true;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCard() {
        return card;
    }

    public void setCard(int card) {
        this.card = card;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getShopping() {
        return shopping;
    }

    public void setShopping(int shopping) {
        this.shopping = shopping;
    }

    public String getCommerce() {
        return commerce;
    }

    public void setCommerce(String commerce) {
        this.commerce = commerce;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public boolean getEticket() {
        return eticket;
    }

    public void setEticket(boolean eticket) {
        this.eticket = eticket;
    }
}

//{
//    "username": "joise",
//    "id_card": 4,
//    "amount": "4991",
//    "buy_order": 44332256,
//    "id_commerce": "10289",
//    "id_store": "001",
//    "installements": "1",
//    "require_eticket": true
//}
