package com.amipass.api.ecommerce_consumer.model.dto.mac;

import java.io.Serializable;

public class OrderConsumerDTO implements Serializable {
    private String UniqueID;
    private Integer AmipassInternalID;
    private String FirstName;
    private String LastName;
    private String Email;
    private String Address;
    private String Commune;
    private String AddressComment;
    private String FmcToken;
    private String MobileNumber;

    public String getUniqueID() {
        return UniqueID;
    }

    public void setUniqueID(String uniqueID) {
        UniqueID = uniqueID;
    }

    public Integer getAmipassInternalID() {
        return AmipassInternalID;
    }

    public void setAmipassInternalID(Integer amipassInternalID) {
        AmipassInternalID = amipassInternalID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAddressComment() {
        return AddressComment;
    }

    public void setAddressComment(String addressComment) {
        AddressComment = addressComment;
    }

    public String getFmcToken() {
        return FmcToken;
    }

    public void setFmcToken(String fmcToken) {
        FmcToken = fmcToken;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getCommune() {
        return Commune;
    }

    public void setCommune(String commune) {
        Commune = commune;
    }
}
