package com.amipass.api.ecommerce_consumer.model.dto.mac;

import java.io.Serializable;
import java.util.Collection;

public class OrderDTO implements Serializable {
    private OrderHeaderDTO orderHeaderDTO;
    private Collection<OrderDetailsDTO> orderDetailsDTO;
    private Collection<OrderPaymentsDTO> orderPaymentsDTO;
    private OrderConsumerDTO orderConsumerDTO;

    public OrderHeaderDTO getOrderHeader() {
        return orderHeaderDTO;
    }

    public void setOrderHeader(OrderHeaderDTO orderHeaderDTO) {
        this.orderHeaderDTO = orderHeaderDTO;
    }

    public Collection<OrderDetailsDTO> getOrderDetails() {
        return orderDetailsDTO;
    }

    public void setOrderDetails(Collection<OrderDetailsDTO> orderDetailsDTO) {
        this.orderDetailsDTO = orderDetailsDTO;
    }

    public Collection<OrderPaymentsDTO> getOrderPayments() {
        return orderPaymentsDTO;
    }

    public void setOrderPayments(Collection<OrderPaymentsDTO> orderPaymentsDTO) {
        this.orderPaymentsDTO = orderPaymentsDTO;
    }

    public OrderConsumerDTO getOrderConsumer() {
        return orderConsumerDTO;
    }

    public void setOrderConsumer(OrderConsumerDTO orderConsumerDTO) {
        this.orderConsumerDTO = orderConsumerDTO;
    }
}
