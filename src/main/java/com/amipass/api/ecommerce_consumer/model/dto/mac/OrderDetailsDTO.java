package com.amipass.api.ecommerce_consumer.model.dto.mac;

import java.io.Serializable;

public class OrderDetailsDTO implements Serializable {
    private Integer ProductID;
    private double TotalItems;
    private double UnitPrice;
    private double DiscountAmount;
    private double FinalAmount;

    public Integer getProductID() {
        return ProductID;
    }

    public void setProductID(Integer productID) {
        ProductID = productID;
    }

    public double getTotalItems() {
        return TotalItems;
    }

    public void setTotalItems(double totalItems) {
        TotalItems = totalItems;
    }

    public double getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        UnitPrice = unitPrice;
    }

    public double getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        DiscountAmount = discountAmount;
    }

    public double getFinalAmount() {
        return FinalAmount;
    }

    public void setFinalAmount(double finalAmount) {
        FinalAmount = finalAmount;
    }
}
