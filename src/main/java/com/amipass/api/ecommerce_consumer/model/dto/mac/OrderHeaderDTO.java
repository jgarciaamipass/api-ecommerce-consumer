package com.amipass.api.ecommerce_consumer.model.dto.mac;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class OrderHeaderDTO implements Serializable {
    private Integer CommerceID;
    private String StoreID;
    private String InternalCommerceID;
    private String TransactionID;
    private Integer PosID;
    private Integer ChannelID;
    private Integer StatusID;
    private Integer OrderTypeID;
    private Integer SellerID;
    private String CreationDate;
    private double TotalAmount;
    private double DiscountAmount;
    private double DeliveryAmount;
    private double FinalAmount;
    private Integer TotalLines;
    private Integer TotalItems;
    private Boolean WasPayByPromotion;

    public Integer getCommerceID() {
        return CommerceID;
    }

    public void setCommerceID(Integer commerceID) {
        CommerceID = commerceID;
    }

    public String getStoreID() {
        return StoreID;
    }

    public void setStoreID(String storeID) {
        StoreID = storeID;
    }

    public String getInternalCommerceID() {
        return InternalCommerceID;
    }

    public void setInternalCommerceID(String internalCommerceID) {
        InternalCommerceID = internalCommerceID;
    }

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public Integer getPosID() {
        return PosID;
    }

    public void setPosID(Integer posID) {
        PosID = posID;
    }

    public Integer getChannelID() {
        return ChannelID;
    }

    public void setChannelID(Integer channelID) {
        ChannelID = channelID;
    }

    public Integer getStatusID() {
        return StatusID;
    }

    public void setStatusID(Integer statusID) {
        StatusID = statusID;
    }

    public Integer getOrderTypeID() {
        return OrderTypeID;
    }

    public void setOrderTypeID(Integer orderTypeID) {
        OrderTypeID = orderTypeID;
    }

    public Integer getSellerID() {
        return SellerID;
    }

    public void setSellerID(Integer sellerID) {
        SellerID = sellerID;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        TotalAmount = totalAmount;
    }

    public double getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        DiscountAmount = discountAmount;
    }

    public double getFinalAmount() {
        return FinalAmount;
    }

    public void setFinalAmount(double finalAmount) {
        FinalAmount = finalAmount;
    }

    public Integer getTotalLines() {
        return TotalLines;
    }

    public void setTotalLines(Integer totalLines) {
        TotalLines = totalLines;
    }

    public Integer getTotalItems() {
        return TotalItems;
    }

    public void setTotalItems(Integer totalItems) {
        TotalItems = totalItems;
    }

    public Boolean getWasPayByPromotion() {
        return WasPayByPromotion;
    }

    public void setWasPayByPromotion(Boolean wasPayByPromotion) {
        WasPayByPromotion = wasPayByPromotion;
    }

    public String getCreationDate() {
        return CreationDate;
    }

    public void setCreationDate(String creationDate) {
        CreationDate = creationDate;
    }

    public double getDeliveryAmount() {
        return DeliveryAmount;
    }

    public void setDeliveryAmount(double deliveryAmount) {
        DeliveryAmount = deliveryAmount;
    }
}
