package com.amipass.api.ecommerce_consumer.model.dto.mac;

import java.io.Serializable;

public class OrderPaymentsDTO implements Serializable {
    private Integer PaymentTypeID;
    private double Amount;
    private Integer CreditCardLastDigit;
    private Integer Quotas;

    public Integer getPaymentTypeID() {
        return PaymentTypeID;
    }

    public void setPaymentTypeID(Integer paymentTypeID) {
        PaymentTypeID = paymentTypeID;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public Integer getCreditCardLastDigit() {
        return CreditCardLastDigit;
    }

    public void setCreditCardLastDigit(Integer creditCardLastDigit) {
        CreditCardLastDigit = creditCardLastDigit;
    }

    public Integer getQuotas() {
        return Quotas;
    }

    public void setQuotas(Integer quotas) {
        Quotas = quotas;
    }
}
