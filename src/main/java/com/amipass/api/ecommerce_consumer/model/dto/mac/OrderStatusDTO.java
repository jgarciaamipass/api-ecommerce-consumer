package com.amipass.api.ecommerce_consumer.model.dto.mac;

import java.io.Serializable;

public class OrderStatusDTO implements Serializable {
    private int status;
    private String reason;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
