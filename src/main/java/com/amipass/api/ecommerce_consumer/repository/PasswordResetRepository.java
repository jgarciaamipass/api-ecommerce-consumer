package com.amipass.api.ecommerce_consumer.repository;

import com.amipass.api.ecommerce_consumer.model.PasswordResetEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordResetRepository extends JpaRepository<PasswordResetEntity, Integer> {
    PasswordResetEntity findByEmailAndActiveIsTrue(String email);
    PasswordResetEntity findByTokenAndActiveIsTrue(String token);
}
