package com.amipass.api.ecommerce_consumer.repository;

import com.amipass.api.ecommerce_consumer.model.PaymentTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentTypeRepository extends JpaRepository<PaymentTypeEntity, Integer> {
}
