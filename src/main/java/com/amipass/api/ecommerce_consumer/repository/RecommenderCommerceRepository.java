package com.amipass.api.ecommerce_consumer.repository;

import com.amipass.api.ecommerce_consumer.model.RecommenderCommerceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecommenderCommerceRepository extends JpaRepository<RecommenderCommerceEntity, Integer> {
}
