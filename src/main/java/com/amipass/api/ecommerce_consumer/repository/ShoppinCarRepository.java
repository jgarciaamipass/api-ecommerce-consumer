package com.amipass.api.ecommerce_consumer.repository;

import com.amipass.api.ecommerce_consumer.model.ShoppingCarEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoppinCarRepository extends JpaRepository<ShoppingCarEntity, Integer> {
    @Query("select car.orderIdMac as order from ShoppingCarEntity car where car.userId = :id and car.status = 2")
    List<Object[]> findOrders(@Param("id") Integer id);

    ShoppingCarEntity findByOrderIdMac(int idOrder);
}
