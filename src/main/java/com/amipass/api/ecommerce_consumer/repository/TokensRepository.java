package com.amipass.api.ecommerce_consumer.repository;

import com.amipass.api.ecommerce_consumer.model.TokensEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokensRepository extends JpaRepository<TokensEntity, Integer> {
    TokensEntity findByTokenAndRevokedIsFalse(String token);
}
