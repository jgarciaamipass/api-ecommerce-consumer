package com.amipass.api.ecommerce_consumer.repository;

import com.amipass.api.ecommerce_consumer.model.UserAccountAmipassEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAccountAmipassRepository extends JpaRepository<UserAccountAmipassEntity, Integer> {
    List<UserAccountAmipassEntity> findByUserId(int userId);
}
