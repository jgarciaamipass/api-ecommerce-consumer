package com.amipass.api.ecommerce_consumer.repository;

import com.amipass.api.ecommerce_consumer.model.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<UsersEntity, Integer> {
    UsersEntity findByEmailAndActiveIsTrue(String email);
    UsersEntity findByEmailAndActiveIsFalse(String email);
    UsersEntity findByEmailAndActiveIsFalseAndCountOfDeclinedEquals(String email, int countOfDeclined);

    @Query("select u.firstName as firstName , u.lastName as lastName, u.numDocument as numDocument, u.phone as phone, u.email as email from UsersEntity u where u.id = :id")
    List<Object[]> findForDelivery(@Param("id") Integer id);

    UsersEntity findByRememberTokenAndActiveIsFalse(String rememberToken);

    UsersEntity findByEmail(String email);
    UsersEntity findByNumDocument(String numDocument);

    UsersEntity findByRememberToken(String rememberToken);
}
