package com.amipass.api.ecommerce_consumer.services;

import com.amipass.api.ecommerce_consumer.model.ChannelShoppingEntity;
import com.amipass.api.ecommerce_consumer.repository.ChannelShoppingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ChannelShoppingService {

    @Autowired
    ChannelShoppingRepository channelShoppingRepository;

    public ResponseEntity<?> all() {
        return ResponseEntity.ok(channelShoppingRepository.findAll());
    }

    public ResponseEntity<?> store(ChannelShoppingEntity channelShoppingEntity) throws ResponseStatusException {
        try {
            return ResponseEntity.ok(channelShoppingRepository.save(channelShoppingEntity));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> show(Integer id) throws ResponseStatusException {
        return ResponseEntity.ok(channelShoppingRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Canal no encontrado")));
    }

    public ResponseEntity<?> update(ChannelShoppingEntity channelShoppingEntity, Integer id)
            throws ResponseStatusException {
        ChannelShoppingEntity channel = channelShoppingRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Canal no encontrado"));
        try {
            channel.setName(channelShoppingEntity.getName());
            return ResponseEntity.ok(channelShoppingRepository.save(channel));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> delete(Integer id) throws ResponseStatusException {
        ChannelShoppingEntity channel = channelShoppingRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Canal no encontrado"));
        try {
            channelShoppingRepository.delete(channel);
            return ResponseEntity.ok(HttpStatus.OK);
        }  catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
