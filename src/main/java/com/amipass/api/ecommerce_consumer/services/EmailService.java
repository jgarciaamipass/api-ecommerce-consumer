package com.amipass.api.ecommerce_consumer.services;

import com.amipass.api.ecommerce_consumer.model.dto.mailer.EmailSenderDTO;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private Configuration configuration;

    public Boolean sendMail(EmailSenderDTO emailSenderDTO, String template, Map<String, Object> model) throws Exception {
        MimeMessage message = sender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

            Template t = configuration.getTemplate(template);
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

//            helper.setFrom("no-reply@amipass.com");
//            helper.setReplyTo("hola@amipass.com");
            helper.setTo(emailSenderDTO.getEmailTo());
            helper.setSubject(emailSenderDTO.getSubject());
            helper.setText(html, true);
            sender.send(message);
            System.out.println("Mail Enviado");
            return true;
        } catch (MessagingException | IOException | TemplateException e) {
            throw new Exception(e.getMessage());
        }
    }
}
