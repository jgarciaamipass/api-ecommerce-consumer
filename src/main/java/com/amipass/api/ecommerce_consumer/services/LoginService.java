package com.amipass.api.ecommerce_consumer.services;

import com.amipass.api.ecommerce_consumer.model.*;
import com.amipass.api.ecommerce_consumer.model.dto.LoginResetPasswordDTO;
import com.amipass.api.ecommerce_consumer.model.dto.mailer.EmailSenderDTO;
import com.amipass.api.ecommerce_consumer.model.dto.users.UniversalUserDTO;
import com.amipass.api.ecommerce_consumer.repository.PasswordResetRepository;
import com.amipass.api.ecommerce_consumer.repository.TokensRepository;
import com.amipass.api.ecommerce_consumer.repository.UsersRepository;
import com.amipass.api.ecommerce_consumer.services.users.UniversalUserService;
import com.amipass.api.ecommerce_consumer.util.JwtUtil;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.security.MessageDigest;

@Service
public class LoginService implements UserDetailsService {

    @Value("${server.amimarket}")
    private String url;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private TokensRepository tokensRepository;

    @Autowired
    private PasswordResetRepository passwordResetRepository;

    @Autowired
    private UniversalUserService universalUserService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private EmailService emailService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public LoginUserDetails loadUserByUsername(String email) {
        UsersEntity user = usersRepository.findByEmailAndActiveIsTrue(email);
        if(user == null) {
            throw new UsernameNotFoundException("Usuario no encontrado");
        }
        return new LoginUserDetails(user);
    }

    public LoginUserDetails loadUserByRememberToken(String key) {
        TokensEntity tokensEntity = tokensRepository.findByTokenAndRevokedIsFalse(key);
        if(tokensEntity != null) {
            UsersEntity user = usersRepository.findById(tokensEntity.getUserId()).get();
            return new LoginUserDetails(user);
        }
        throw new UsernameNotFoundException("Token no válido");
    }

    private String createMD5(String value) throws Exception {
        byte[] byteOfToken = value.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] tokenDigest = md.digest(byteOfToken);
        StringBuilder sb = new StringBuilder();
        for (byte b : tokenDigest) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public ResponseEntity<?> login(AuthenticationRequest authenticationRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getEmail(),
                            authenticationRequest.getPassword()
                    )
            );
        } catch (BadCredentialsException e) {
            UsersEntity usersEntity = usersRepository.findByEmail(authenticationRequest.getEmail());
            if(usersEntity != null) {
                if(usersEntity.getCountOfDeclined() > 3) {
                    usersEntity.setActive(false);
                    usersRepository.save(usersEntity);
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Usuario bloqueado");
                }
                usersEntity.setCountOfDeclined(usersEntity.getCountOfDeclined() + 1);
                usersRepository.save(usersEntity);
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Usuario o clave inválido");
            }
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no existe");
        }

        final LoginUserDetails loginUserDetails =
                this.loadUserByUsername(authenticationRequest.getEmail());

        final String jwt = jwtUtil.generateToken(loginUserDetails);
        String jwtUniversalUser;
        try {
            UniversalUserDTO userDTO = new UniversalUserDTO();
            userDTO.setUsername(authenticationRequest.getEmail());
            userDTO.setPassword(authenticationRequest.getPassword());
            jwtUniversalUser = universalUserService.login(userDTO).getBody().toString();
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "");
        }

        UsersEntity user = usersRepository.findByEmailAndActiveIsTrue(authenticationRequest.getEmail());

        if(user == null) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Usuario bloqueado");
        }

        if(authenticationRequest.getRemember()) {
            String md5 = this.createMD5(LocalDateTime.now().toString());
            user.setRememberToken(md5);
            TokensEntity tokensEntity = new TokensEntity();
            tokensEntity.setUserId(user.getId());
            tokensEntity.setToken(md5);
            tokensEntity.setRevoked(false);
            tokensEntity.setCreatedAt(Date.valueOf(LocalDate.now()));
            tokensEntity.setUpdatedAt(Date.valueOf(LocalDate.now()));
            tokensRepository.save(tokensEntity);
        }

        user.setCountOfDeclined(0);
        usersRepository.save(user);

        Map<String, Object> result = new HashMap<>();

        result.put("jwt", jwt);
        result.put("iduser", user.getId());
        result.put("fullname", String.format("%s %s", user.getFirstName(), user.getLastName()));
        result.put("rememberToken", (authenticationRequest.getRemember()) ? user.getRememberToken() : null);
        result.put("jwtMAC", jwtUniversalUser);

        return ResponseEntity.ok(result);
    }

    public ResponseEntity<?> register(UsersEntity usersEntity) throws Exception {
        try {
            usersEntity.setCreatedAt(Date.valueOf(LocalDate.now()));
            usersEntity.setUpdatedAt(Date.valueOf(LocalDate.now()));
            String md5 = this.createMD5(LocalDateTime.now().toString());
            usersEntity.setRememberToken(md5);
            usersEntity.setActive(false);
            usersEntity.setPassword(encoder.encode(usersEntity.getPassword()));
            usersEntity.setCountOfDeclined(0);

            usersRepository.save(usersEntity);

            EmailSenderDTO emailSenderDTO = new EmailSenderDTO();

            emailSenderDTO.setEmailTo(usersEntity.getEmail());
            emailSenderDTO.setSubject("Activa tu cuenta amiMARKET");

            Map<String, Object> model = new HashMap<>();

            model.put("fullname", String.format("%s %s", usersEntity.getFirstName(), usersEntity.getLastName()));
            model.put("link", String.format("%s/activate_account/%s", this.url, md5));
            model.put("email", usersEntity.getEmail());

            Map<String, Object> userRegistered = new HashMap<>();
            userRegistered.put("username", usersEntity.getId());
            userRegistered.put("email", usersEntity.getEmail());

            emailService.sendMail(emailSenderDTO, "/VerifyEmail/index.ftl", model);

            return ResponseEntity.ok(userRegistered);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> active_account(String token) throws Exception {
        UsersEntity usersEntity = usersRepository.findByRememberTokenAndActiveIsFalse(token);

        if(usersEntity == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no existe o ya está activo");
        }

        try {
            usersEntity.setActive(true);
            usersRepository.save(usersEntity);

            EmailSenderDTO emailSenderDTO = new EmailSenderDTO();

            emailSenderDTO.setEmailTo(usersEntity.getEmail());
            emailSenderDTO.setSubject("Bienvenido a amiMARKET");

            Map<String, Object> model = new HashMap<>();

            model.put("fullname", String.format("%s %s", usersEntity.getFirstName(), usersEntity.getLastName()));
            model.put("email", usersEntity.getEmail());

            emailService.sendMail(emailSenderDTO, "/Welcome/index.ftl", model);

            return ResponseEntity.ok(usersEntity);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> forgotPassword(String email) throws Exception {
        if(usersRepository.findByEmail(email) == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado");
        }
        UsersEntity usersEntity = usersRepository.findByEmailAndActiveIsTrue(email);

        if(usersEntity == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Usuario no activo");
        } else {
            PasswordResetEntity sended = passwordResetRepository.findByEmailAndActiveIsTrue(email);
            if(sended == null) {
                PasswordResetEntity passwordResetEntity = new PasswordResetEntity();
                String md5 = this.createMD5(LocalDateTime.now().toString());
                passwordResetEntity.setToken(md5);
                passwordResetEntity.setCreatedAt(Date.valueOf(LocalDate.now()));
                passwordResetEntity.setEmail(usersEntity.getEmail());
                passwordResetEntity.setActive(true);

                passwordResetRepository.save(passwordResetEntity);

                EmailSenderDTO emailSenderDTO = new EmailSenderDTO();

                emailSenderDTO.setEmailTo(usersEntity.getEmail());
                emailSenderDTO.setSubject("Recuperar mi contraseña");

                Map<String, Object> model = new HashMap<>();

                model.put("fullname", String.format("%s %s", usersEntity.getFirstName(), usersEntity.getLastName()));
                model.put("link", String.format("%s/forget_password/%s", this.url, passwordResetEntity.getToken()));
                model.put("email", usersEntity.getEmail());
                emailService.sendMail(emailSenderDTO, "/ForgotPassword/index.ftl", model);
                usersEntity.setPassword(usersEntity.getPassword());
                return ResponseEntity.ok(HttpStatus.OK);
            } else {
                throw new ResponseStatusException(HttpStatus.ALREADY_REPORTED, "Email ya enviado");
            }
        }
    }

    public ResponseEntity<?> resetPassword(LoginResetPasswordDTO loginResetPasswordDTO) throws Exception {
        try {
            if(!loginResetPasswordDTO.getPassword().equals(loginResetPasswordDTO.getPasswordConfirm())) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Las contraseñas no son iguales.");
            }

            PasswordResetEntity passwordResetEntity = passwordResetRepository.findByTokenAndActiveIsTrue(loginResetPasswordDTO.getToken());

            if(passwordResetEntity == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Token no válido");
            } else {
                UsersEntity usersEntity = usersRepository.findByEmailAndActiveIsTrue(passwordResetEntity.getEmail());
                if(usersEntity == null) {
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "El usuario no está activo");
                } else {
                    usersEntity.setPassword(encoder.encode(loginResetPasswordDTO.getPassword()));
                    usersEntity.setUpdatedAt(Date.valueOf(LocalDate.now()));
                    usersEntity.setCountOfDeclined(0);
                    usersEntity.setActive(true);

                    String md5 = this.createMD5(LocalDateTime.now().toString());
                    usersEntity.setRememberToken(md5);

                    passwordResetEntity.setActive(false);
                    usersRepository.save(usersEntity);
                    passwordResetRepository.save(passwordResetEntity);

                    EmailSenderDTO emailSenderDTO = new EmailSenderDTO();

                    emailSenderDTO.setEmailTo(usersEntity.getEmail());
                    emailSenderDTO.setSubject("Contraseña restablecida");

                    Map<String, Object> model = new HashMap<>();

                    model.put("fullname", String.format("%s %s", usersEntity.getFirstName(), usersEntity.getLastName()));
                    model.put("email", usersEntity.getEmail());
                    emailService.sendMail(emailSenderDTO, "/PasswordChanged/index.ftl", model);
                    return ResponseEntity.ok(HttpStatus.OK);
                }
            }
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> refreshToken(HttpServletRequest request) throws Exception {
        // From the HttpRequest get the claims
        DefaultClaims claims = (io.jsonwebtoken.impl.DefaultClaims) request.getAttribute("claims");
        if(claims != null) {
            Map<String, Object> expectedMap = getMapFromIoJsonwebtokenClaims(claims);
            UsersEntity usersEntity = usersRepository.findByEmailAndActiveIsTrue(expectedMap.get("sub").toString());
            if(usersEntity == null) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "El usuario no está activo");
            } else {
                String token = jwtUtil.refreshToken(expectedMap, usersEntity.getEmail());
            return ResponseEntity.ok(new AuthenticationResponse(token, usersEntity.getId(), String.format("%s %s", usersEntity.getFirstName(), usersEntity.getLastName())));
            }
        } else {
            return ResponseEntity.status(302).body("");
        }
    }

    public Map<String, Object> getMapFromIoJsonwebtokenClaims(DefaultClaims claims) {
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : claims.entrySet()) {
            expectedMap.put(entry.getKey(), entry.getValue());
        }
        return expectedMap;
    }
}
