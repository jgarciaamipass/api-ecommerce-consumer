package com.amipass.api.ecommerce_consumer.services;

import com.amipass.api.ecommerce_consumer.exception.ResourceNotFoundException;
import com.amipass.api.ecommerce_consumer.model.PaymentTypeEntity;
import com.amipass.api.ecommerce_consumer.repository.PaymentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PaymentTypeService {

    @Autowired
    PaymentTypeRepository paymentTypeRepository;

    public List<PaymentTypeEntity> all() {
        return paymentTypeRepository.findAll();
    }

    public ResponseEntity<?> store(PaymentTypeEntity paymentTypeEntity) throws ResponseStatusException {
        try {
            return ResponseEntity.ok(paymentTypeRepository.save(paymentTypeEntity));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> show(Integer id) throws ResponseStatusException {
        return ResponseEntity.ok(paymentTypeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Medio de pago no encontrado")));
    }

    public ResponseEntity<?> update(PaymentTypeEntity paymentTypeEntity, Integer id) throws ResponseStatusException {
        PaymentTypeEntity payment = paymentTypeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Medio de pago no encontrado"));
        try {
            payment.setName(paymentTypeEntity.getName());
            return ResponseEntity.ok(paymentTypeRepository.save(payment));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> delete(Integer id)
            throws ResponseStatusException {
        PaymentTypeEntity paymentTypeEntity = paymentTypeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Medio de pago no encontrado"));
        try {
            paymentTypeRepository.delete(paymentTypeEntity);
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
