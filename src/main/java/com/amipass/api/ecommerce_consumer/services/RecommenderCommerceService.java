package com.amipass.api.ecommerce_consumer.services;

import com.amipass.api.ecommerce_consumer.model.RecommenderCommerceEntity;
import com.amipass.api.ecommerce_consumer.repository.RecommenderCommerceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Date;
import java.time.LocalDate;

@Service
public class RecommenderCommerceService {

    @Autowired
    private RecommenderCommerceRepository recommenderCommerceRepository;

    public ResponseEntity<?> all() {
        return ResponseEntity.ok(HttpStatus.OK);
    }

    public ResponseEntity<RecommenderCommerceEntity> show(int id) {
        try {
            return ResponseEntity.ok(recommenderCommerceRepository.findById(id)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Información no encontrada")));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> store(RecommenderCommerceEntity recommenderCommerceEntity) {
        try {
            recommenderCommerceEntity.setCreatedAt(Date.valueOf(LocalDate.now()));
            recommenderCommerceRepository.save(recommenderCommerceEntity);
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
