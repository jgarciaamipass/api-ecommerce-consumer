package com.amipass.api.ecommerce_consumer.services;

import com.amipass.api.ecommerce_consumer.model.ShoppingCarEntity;
import com.amipass.api.ecommerce_consumer.repository.ShoppinCarRepository;
import com.amipass.api.ecommerce_consumer.services.mac.OrderService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShoppingCarService {

    @Autowired
    private ShoppinCarRepository shoppinCarRepository;

    @Autowired
    private OrderService orderService;

    public ResponseEntity<?> all(int id) throws Exception {
        List<Object[]> records = shoppinCarRepository.findOrders(id);
        List<Object> orders = new ArrayList<>();

        for (Object[] order :
                records) {
            try {
                JsonObject jsonObject = JsonParser.parseString(orderService.show(Integer.parseInt(order[0].toString())).getBody().toString()).getAsJsonObject();
                orders.add(jsonObject);
            } catch (Exception e) {
                System.out.println(String.format("Orden %s no encontrada en MAC para el usuario %s", order[0], id));
            }
        }

        return ResponseEntity.ok(orders.toString());
    }

    public ResponseEntity<?> store(ShoppingCarEntity shoppingCarEntity) throws ResponseStatusException {
        try {
            shoppingCarEntity.setCreatedAt(Date.valueOf(LocalDate.now()));
            shoppingCarEntity.setUpdatedAt(Date.valueOf(LocalDate.now()));
            return ResponseEntity.ok(shoppinCarRepository.save(shoppingCarEntity));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> show(Integer id) throws ResponseStatusException {
        return ResponseEntity.ok(shoppinCarRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Carro no encontrado")));
    }

    public ResponseEntity<?> update(ShoppingCarEntity shoppingCarEntity, Integer id) throws ResponseStatusException {
        try {
            ShoppingCarEntity shopping = shoppinCarRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Carro no encontrado"));
            shopping.setComment(shoppingCarEntity.getComment());
            shopping.setDeliveryAmount(shoppingCarEntity.getDeliveryAmount());
            shopping.setDiscountAmount(shoppingCarEntity.getDiscountAmount());
            shopping.setChannelShoppingId(shoppingCarEntity.getChannelShoppingId());
            shopping.setCommerce(shoppingCarEntity.getCommerce());
            shopping.setPaymentTypeId(shoppingCarEntity.getPaymentTypeId());
            shopping.setId(shoppingCarEntity.getId());
            shopping.setStore(shoppingCarEntity.getStore());
            shopping.setUserId(shoppingCarEntity.getUserId());
            shopping.setUserAddressId(shoppingCarEntity.getUserAddressId());
            shopping.setStatus(shoppingCarEntity.getStatus());
            shopping.setTotalAmount(shoppingCarEntity.getTotalAmount());
            shopping.setUpdatedAt(Date.valueOf(LocalDate.now()));
            return ResponseEntity.ok(shoppinCarRepository.save(shopping));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> delete(Integer id) throws ResponseStatusException {
        ShoppingCarEntity shoppingCarEntity = shoppinCarRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Carro no encontrado"));
        try {
            shoppinCarRepository.delete(shoppingCarEntity);
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
