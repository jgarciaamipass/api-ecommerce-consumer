package com.amipass.api.ecommerce_consumer.services;

import com.amipass.api.ecommerce_consumer.model.UserAddressEntity;
import com.amipass.api.ecommerce_consumer.repository.UserAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Date;
import java.time.LocalDate;

@Service
public class UserAddressService {

    @Autowired
    UserAddressRepository userAddressRepository;

    public ResponseEntity<?> all(){
        return ResponseEntity.ok(userAddressRepository.findAll());
    }

    public ResponseEntity<?> store(UserAddressEntity userAddressEntity){
        try {
            userAddressEntity.setCreatedAt(Date.valueOf(LocalDate.now()));
            userAddressEntity.setUpdatedAt(Date.valueOf(LocalDate.now()));
            return ResponseEntity.ok(userAddressRepository.save(userAddressEntity));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> show(Integer id) throws ResponseStatusException {
        return ResponseEntity.ok(userAddressRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Dirección no encontrada")));
    }

    public ResponseEntity<?> update(UserAddressEntity userAddressEntity, Integer id)
            throws ResponseStatusException {
        UserAddressEntity address = userAddressRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Dirección no encontrada"));
        try {
            address.setUpdatedAt(Date.valueOf(LocalDate.now()));
            address.setAddress(userAddressEntity.getAddress());
            return ResponseEntity.ok(userAddressRepository.save(address));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> delete(Integer id) throws ResponseStatusException {
        UserAddressEntity address = userAddressRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Dirección no encontrada"));
        try {
            userAddressRepository.delete(address);
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> showByIdUser(Integer id) {
        return ResponseEntity.ok(userAddressRepository.findByUserId(id));
    }
}
