package com.amipass.api.ecommerce_consumer.services;

import com.amipass.api.ecommerce_consumer.model.UsersEntity;
import com.amipass.api.ecommerce_consumer.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public ResponseEntity<?> all() {
        return ResponseEntity.ok(usersRepository.findAll());
    }

    public ResponseEntity<?> store(UsersEntity usersEntity) throws ResponseStatusException {
        try {
            usersEntity.setCreatedAt(Date.valueOf(LocalDate.now()));
            usersEntity.setUpdatedAt(Date.valueOf(LocalDate.now()));
            usersEntity.setActive(true);
            usersEntity.setPassword(encoder.encode(usersEntity.getPassword()));
            usersEntity.setPassword(usersEntity.getPassword());
            return ResponseEntity.ok(usersRepository.save(usersEntity));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> show(Integer id) throws ResponseStatusException {
        return ResponseEntity.ok(usersRepository
                .findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado")));
    }

    public ResponseEntity<?> findByEmail(String email) throws ResponseStatusException {
        try {
            return ResponseEntity.ok(usersRepository.findByEmailAndActiveIsTrue(email));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> update(UsersEntity user, Integer id) throws ResponseStatusException {
        try {
            UsersEntity usersEntity = usersRepository.findById(id)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado"));
            usersEntity.setFirstName(user.getFirstName());
            usersEntity.setLastName(user.getLastName());
            usersEntity.setEmail(user.getEmail());
            usersEntity.setBirthday(user.getBirthday());
            usersEntity.setPhone(user.getPhone());
            usersEntity.setNumDocument(user.getNumDocument());
            usersEntity.setUpdatedAt(Date.valueOf(LocalDate.now()));
            return ResponseEntity.ok(usersRepository.save(usersEntity));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> delete(Integer id) throws ResponseStatusException {
        UsersEntity user = usersRepository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado"));
        usersRepository.delete(user);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    public ResponseEntity<?> infoDelivery(Integer id) throws ResponseStatusException {
        try {
            Map<String, Object> user = new HashMap<>();
            List<Object[]> records = usersRepository.findForDelivery(id);

            Object[] object = records.get(0);

            user.put("firstName", String.valueOf(object[0]));
            user.put("lastName", String.valueOf(object[1]));
            user.put("numDocument", String.valueOf(object[2]));
            user.put("phone", String.valueOf(object[3]));
            user.put("email", String.valueOf(object[4]));

            return ResponseEntity.ok(user);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> emailExist(String email) throws ResponseStatusException {
        try {
            UsersEntity usersEntity = usersRepository.findByEmail(email);
            if(usersEntity == null) {
                return ResponseEntity.ok(HttpStatus.OK);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> documentExist(String document) throws ResponseStatusException {
        try {
            UsersEntity usersEntity = usersRepository.findByNumDocument(document);
            if(usersEntity == null) {
                return ResponseEntity.ok(HttpStatus.OK);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

}
