package com.amipass.api.ecommerce_consumer.services.amipass;

import com.amipass.api.ecommerce_consumer.model.UserAccountAmipassEntity;
import com.amipass.api.ecommerce_consumer.model.dto.amipass.AccountAmipassDTO;
import com.amipass.api.ecommerce_consumer.model.dto.amipass.AccountAmipassLoginDTO;
import com.amipass.api.ecommerce_consumer.repository.UserAccountAmipassRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

@Service
public class AccountAmipassService {

    @Value("${server.amipass}")
    private String uri;

    @Autowired
    UserAccountAmipassRepository userAccountAmipassRepository;

    public ResponseEntity<?> showFromAmipass(AccountAmipassLoginDTO accountAmipassLoginDTO) throws Exception {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Map<String, String> params = new HashMap<>();

            AmipassAppRed amipassAppRed = new AmipassAppRed(accountAmipassLoginDTO, this.uri);
            System.out.println(amipassAppRed.getToken());
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(amipassAppRed.getToken());

            URI uri = new URI(this.uri);
            params.put("user", amipassAppRed.getAccountAmipassLoginDTO().getUsername());

            HttpEntity<Object> request = new HttpEntity<>(params, headers);

            JsonObject jsonResult = JsonParser.parseString(Objects.requireNonNull(restTemplate.postForObject(uri, request, String.class))).getAsJsonObject();
            JsonArray jsonArray = JsonParser.parseString(jsonResult.get("data").toString()).getAsJsonArray();
            List<Object> accounts = new ArrayList<>();
            for (JsonElement item : jsonArray)
            {
                JsonObject obj = item.getAsJsonObject();
                AccountAmipassDTO accountDTO = new AccountAmipassDTO();
                accountDTO.setUsername(obj.get("username").getAsString());
                accountDTO.setFirstName(obj.get("firstname").getAsString());
                accountDTO.setLastName(obj.get("lastname").getAsString());
                accountDTO.setLastName2(obj.get("lastname2").getAsString());
                accountDTO.setEmployer(obj.get("employer").getAsString());
                accountDTO.setEmployerName(obj.get("employerName").getAsString());
                accountDTO.setCommercialName(obj.get("commercialName").getAsString());
                accountDTO.setNumberCard(obj.get("card").getAsString());
                accountDTO.setBalance(obj.get("balance").getAsLong());
                accountDTO.setStatus(obj.get("status").getAsString());
                accounts.add(accountDTO);
            }
            return ResponseEntity.ok(accounts);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> show(Integer id) {
        try {
            return ResponseEntity.ok(userAccountAmipassRepository.findByUserId(id));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> store(List<AccountAmipassDTO> account, Integer id) {
        try {
            for (AccountAmipassDTO item : account ) {
                UserAccountAmipassEntity userAccountAmipassEntity = new UserAccountAmipassEntity();
                userAccountAmipassEntity.setEmployer(item.getEmployer());
                userAccountAmipassEntity.setEmployerName(String.format("%s (%s)", item.getEmployerName(), item.getCommercialName()));
                userAccountAmipassEntity.setEmployee(item.getUsername());
                userAccountAmipassEntity.setEmployeeName(String.format("%s %s %s", item.getFirstName(), item.getLastName(), item.getLastName2()));
                userAccountAmipassEntity.setCreatedAt(Date.valueOf(LocalDate.now()));
                userAccountAmipassEntity.setUpdatedAt(Date.valueOf(LocalDate.now()));
                userAccountAmipassEntity.setCardCode(item.getNumberCard());
                userAccountAmipassEntity.setUserId(id);
                userAccountAmipassRepository.save(userAccountAmipassEntity);
            }
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
