package com.amipass.api.ecommerce_consumer.services.amipass;

import com.amipass.api.ecommerce_consumer.model.dto.amipass.AccountAmipassLoginDTO;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AmipassAppRed {

    private AccountAmipassLoginDTO accountAmipassLoginDTO;
    private String token;

    public AmipassAppRed(AccountAmipassLoginDTO accountAmipassLoginDTO, String uri) throws ResponseStatusException {
        LoginAmipass(accountAmipassLoginDTO, uri);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AccountAmipassLoginDTO getAccountAmipassLoginDTO() {
        return accountAmipassLoginDTO;
    }

    public void setAccountAmipassLoginDTO(AccountAmipassLoginDTO accountAmipassLoginDTO) {
        this.accountAmipassLoginDTO = accountAmipassLoginDTO;
    }

    private void LoginAmipass(AccountAmipassLoginDTO account, String uri) throws ResponseStatusException {
        try {
            String uriLogin = String.format("%s/login", uri);
            Map<String, String> paramsLogin = new HashMap<>();
            paramsLogin.put("username", account.getUsername());
            paramsLogin.put("password", account.getPassword());
            RestTemplate restTemplate = new RestTemplate();
            JsonObject jsonLogin = JsonParser.parseString(Objects.requireNonNull(restTemplate.postForObject(uriLogin, paramsLogin, String.class))).getAsJsonObject();
            this.setToken(JsonParser.parseString(jsonLogin.get("data").toString()).getAsJsonObject().get("token").toString().replace("\"", ""));
            this.setAccountAmipassLoginDTO(account);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }
}
