package com.amipass.api.ecommerce_consumer.services.finance;

import com.amipass.api.ecommerce_consumer.model.ShoppingCarEntity;
import com.amipass.api.ecommerce_consumer.model.ShoppingCarItemEntity;
import com.amipass.api.ecommerce_consumer.model.dto.finance.PayAmipassDTO;
import com.amipass.api.ecommerce_consumer.model.dto.mac.OrderDetailsDTO;
import com.amipass.api.ecommerce_consumer.repository.ShoppinCarRepository;
import com.amipass.api.ecommerce_consumer.services.mac.CommerceService;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.*;

@Service
public class PaymentAmipassService {

    @Value("${server.finance}")
    private String uri;

    @Autowired
    private ShoppinCarRepository shoppinCarRepository;

    @Autowired
    private CommerceService commerceService;

    public ResponseEntity<?> pay(PayAmipassDTO payAmipassDTO) throws Exception {
        try {
            ShoppingCarEntity carEntity = shoppinCarRepository
                    .findById(Integer.parseInt(payAmipassDTO.getBuyOrder()))
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Carro de compra no encontrado"));

            JsonElement jsonElementIsOpen = JsonParser.parseString(commerceIsOpen(String.format("%s%s", carEntity.getCommerce(), carEntity.getStore())).toString());
            JsonObject jsonObjectIsOpen = jsonElementIsOpen.getAsJsonObject();

            if(!jsonObjectIsOpen.get("isOpen").getAsBoolean()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Comercio no disponible");
            }

            List<Object> listProductForValidateStock = new ArrayList<>();
            for (ShoppingCarItemEntity item :
                    carEntity.getShoppingCarItemsByShoppingCarId()) {
                Map<String, Object> productForValidateStock = new HashMap<>();
                productForValidateStock.put("id", item.getProductIdMac());
                productForValidateStock.put("stock", item.getQuantity());
                listProductForValidateStock.add(productForValidateStock);
            }

            JsonElement jsonElementValidateProducts = JsonParser.parseString(validateStock(String.format("%s%s", carEntity.getCommerce(), carEntity.getStore()), listProductForValidateStock).toString());
            JsonObject jsonObjectValidateProducts = jsonElementValidateProducts.getAsJsonObject();

            if(jsonObjectValidateProducts.getAsJsonArray("productStocks").size() > 0) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Stock no disponible para uno de los productos");
            }

            carEntity.setTryPay(carEntity.getTryPay() + 1);
            shoppinCarRepository.save(carEntity);

            RestTemplate restTemplate = new RestTemplate();
            Map<String, Object> params = new HashMap<>();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            URI uri = new URI(String.format("%s/paymentamipass/payment",this.uri));

            params.put("sEmpleado", payAmipassDTO.getsEmpleado());
            params.put("sEmpresa", payAmipassDTO.getsEmpresa());
            params.put("sClave", payAmipassDTO.getsClave());
            params.put("sEstablecimiento", payAmipassDTO.getsEstablecimiento());
            params.put("sLocal", payAmipassDTO.getsLocal());
            params.put("nMonto", payAmipassDTO.getnMonto());
            params.put("buyOrder", String.format("e%s-%s", payAmipassDTO.getBuyOrder(), carEntity.getTryPay()));

            HttpEntity<Object> request = new HttpEntity<>(params, headers);

            return ResponseEntity.ok(Objects.requireNonNull(restTemplate.postForObject(uri, request, String.class)));
        } catch (ResponseStatusException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private Object validateStock(String commerce, Object products) {
        try {
            return commerceService.validateProducts(commerce, products).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Object commerceIsOpen(String commerce) {
        try {
            return commerceService.isOpen(commerce).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
