package com.amipass.api.ecommerce_consumer.services.finance;


import com.amipass.api.ecommerce_consumer.model.ShoppingCarEntity;
import com.amipass.api.ecommerce_consumer.model.ShoppingCarItemEntity;
import com.amipass.api.ecommerce_consumer.model.dto.finance.AccountTransbankDTO;
import com.amipass.api.ecommerce_consumer.model.dto.finance.PayTransbankDTO;
import com.amipass.api.ecommerce_consumer.repository.ShoppinCarRepository;
import com.amipass.api.ecommerce_consumer.services.mac.CommerceService;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Service
public class PaymentTransbankService {

    @Value("${server.finance}")
    private String uri;

    @Autowired
    private ShoppinCarRepository shoppinCarRepository;

    @Autowired
    private CommerceService commerceService;

    public ResponseEntity<?> cards(AccountTransbankDTO accountTransbankDTO) throws Exception {
        try {
            RestTemplate restTemplate = new RestTemplate();
            URI uri = new URI(String.format("%s/inscription/%s/%s", this.uri, accountTransbankDTO.getUsername(), accountTransbankDTO.getEmail()));
            return restTemplate.getForEntity(uri, String.class);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> subscribe(AccountTransbankDTO accountTransbankDTO) throws Exception {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Map<String, String> params = new HashMap<>();
            URI uri = new URI(String.format("%s/inscription/create", this.uri));

            HttpHeaders headers = new HttpHeaders();

            params.put("email", accountTransbankDTO.getEmail());
            params.put("username", accountTransbankDTO.getUsername());
            params.put("response_url", accountTransbankDTO.getResponseUrl());

            HttpEntity<Object> request = new HttpEntity<>(params, headers);

            return ResponseEntity.ok(Objects.requireNonNull(restTemplate.postForObject(uri, request, String.class)));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> unsubscribe(AccountTransbankDTO accountTransbankDTO) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = new URI(String.format("%s/inscription/delete/%s/%s", this.uri, accountTransbankDTO.getUsername(), accountTransbankDTO.getId()));
        restTemplate.delete(uri);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    public ResponseEntity<?> pay(PayTransbankDTO payTransbankDTO) throws Exception {
        try {
            ShoppingCarEntity carEntity = shoppinCarRepository
                    .findById(payTransbankDTO.getShopping())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Carro de compra no encontrado"));

            JsonElement jsonElementIsOpen = JsonParser.parseString(commerceIsOpen(String.format("%s%s", carEntity.getCommerce(), carEntity.getStore())).toString());
            JsonObject jsonObjectIsOpen = jsonElementIsOpen.getAsJsonObject();

            if(!jsonObjectIsOpen.get("isOpen").getAsBoolean()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Comercio no disponible");
            }

            List<Object> listProductForValidateStock = new ArrayList<>();
            for (ShoppingCarItemEntity item :
                    carEntity.getShoppingCarItemsByShoppingCarId()) {
                Map<String, Object> productForValidateStock = new HashMap<>();
                productForValidateStock.put("id", item.getProductIdMac());
                productForValidateStock.put("stock", item.getQuantity());
                listProductForValidateStock.add(productForValidateStock);
            }

            JsonElement jsonElementValidateProducts = JsonParser.parseString(validateStock(String.format("%s%s", carEntity.getCommerce(), carEntity.getStore()), listProductForValidateStock).toString());
            JsonObject jsonObjectValidateProducts = jsonElementValidateProducts.getAsJsonObject();

            if(jsonObjectValidateProducts.getAsJsonArray("productStocks").size() > 0) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Stock no disponible para uno de los productos");
            }

            carEntity.setTryPay(carEntity.getTryPay() + 1);
            shoppinCarRepository.save(carEntity);

            RestTemplate restTemplate = new RestTemplate();
            Map<String, Object> params = new HashMap<>();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            URI uri = new URI(String.format("%s/payment/create", this.uri));

            params.put("username", payTransbankDTO.getUsername());
            params.put("id_card", payTransbankDTO.getCard());
            params.put("amount", payTransbankDTO.getAmount());
            params.put("buy_order", String.format("e%s-%s", payTransbankDTO.getShopping(), carEntity.getTryPay()));
            params.put("id_commerce", payTransbankDTO.getCommerce());
            params.put("id_store", payTransbankDTO.getStore());
            params.put("installements", payTransbankDTO.getInstallments());
            params.put("require_eticket", payTransbankDTO.getEticket());

//            return ResponseEntity.ok(params);

            HttpEntity<Object> request = new HttpEntity<>(params, headers);

            JsonObject jsonOrder = JsonParser.parseString(Objects.requireNonNull(restTemplate.postForObject(uri, request, String.class))).getAsJsonObject();

            String codeResponse = jsonOrder.get("code_response").getAsString();

            if(codeResponse.equals("0")) {
                JsonObject jsonHeader = jsonOrder.get("eTicket").getAsJsonObject();
                String urlOriginal = jsonHeader.get("urlOriginal").getAsString();
                carEntity.setUrlInvoice(urlOriginal);
                shoppinCarRepository.save(carEntity);
            }

            return ResponseEntity.ok(jsonOrder.toString());
        } catch (ResponseStatusException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private Object validateStock(String commerce, Object products) {
        try {
            return commerceService.validateProducts(commerce, products).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Object commerceIsOpen(String commerce) {
        try {
            return commerceService.isOpen(commerce).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
