package com.amipass.api.ecommerce_consumer.services.mac;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.*;

@Service
public class CommerceService extends MAC {

    public ResponseEntity<?> all(FinderParams params) throws Exception {
        try {
            URI uriPickUp = new URI(String.format("%s/commerces/by/with/pick-up?page=%s&chunk=%s&lat=%s&lng=%s&radius=%s", this.uri, params.getPage(), params.getChunk(), params.getLatitude(), params.getLongitude(), params.getRadius()));

            params.setCommune(params.getCommune().replace(" ", "%20"));
            URI uriCommune = new URI(String.format("%s/commerces/by/delivery/commune/%s?page=%s&chunk=%s", this.uri, params.getCommune(), params.getPage(), params.getChunk()));

            List<Object> results = new ArrayList<>();

            JsonElement jsonElementWithdrawal = JsonParser.parseString(this.api(uriPickUp, HttpMethod.GET).getBody().toString());
            JsonElement jsonElementDelivery = JsonParser.parseString(this.api(uriCommune, HttpMethod.GET).getBody().toString());

            JsonArray jsonArrayWithdrawal = jsonElementWithdrawal.getAsJsonArray();
            JsonArray jsonArrayDelivery = jsonElementDelivery.getAsJsonArray();

            double lat1 = Double.parseDouble(params.getLatitude());
            double lng1 = Double.parseDouble(params.getLongitude());

            if(params.getKey().length() > 0) {
                params.setKey(params.getKey().replace(" ", "%20"));
                URI uriNameCommerce = new URI(String.format("%s/commerces/by/commerce/name/%s?page=%s&chunk=%s&lat=%s&lng=%s&radius=%s", this.uri, params.getKey(), params.getPage(), params.getChunk(), params.getLatitude(), params.getLongitude(), params.getRadius()));
                URI uriNameProduct = new URI(String.format("%s/commerces/by/product/name/%s?page=%s&chunk=%s&lat=%s&lng=%s&radius=%s", this.uri, params.getKey(), params.getPage(), params.getChunk(), params.getLatitude(), params.getLongitude(), params.getRadius()));

                JsonElement jsonElementCommerce = JsonParser.parseString(this.api(uriNameCommerce, HttpMethod.GET).getBody().toString());
                JsonElement jsonElementProduct = JsonParser.parseString(this.api(uriNameProduct, HttpMethod.GET).getBody().toString());

                JsonArray jsonArrayCommerce = jsonElementCommerce.getAsJsonArray();
                JsonArray jsonArrayProduct = jsonElementProduct.getAsJsonArray();

                List<JsonObject> resultsCommerce = new ArrayList<>();
                List<JsonObject> resultsProduct = new ArrayList<>();

                for (int i = 0; i < jsonArrayCommerce.size(); i++) {
                    JsonObject item = jsonArrayCommerce.get(i).getAsJsonObject();
                    resultsCommerce.add(item);
                }

                for (int i = 0; i < jsonArrayProduct.size(); i++) {
                    JsonObject item = jsonArrayProduct.get(i).getAsJsonObject();
                    if(resultsCommerce.stream().filter(commerce -> commerce.equals(item)).count() > 0) continue;
                    resultsProduct.add(item);
                }

                results.addAll(resultsCommerce);
                results.addAll(resultsProduct);

                if(params.getDelivery()) {
                    List<JsonObject> resultsDeliveryByName = new ArrayList<>();
                    for (int i = 0; i < jsonArrayDelivery.size(); i++) {
                        JsonObject item = jsonArrayDelivery.get(i).getAsJsonObject();
                        if(results.stream().filter(commerce -> commerce.equals(item)).count() > 0) {
                            item.addProperty("distance", this.distance(lat1,lng1,item.get("lat").getAsDouble(),item.get("lng").getAsDouble()));
                            if(item.get("hasDelivery").getAsBoolean()) {
                                item.addProperty("hasWithdrawal", false);
                                resultsDeliveryByName.add(item);
                            }
                        }
                    }
                    results.clear();
                    results.addAll(resultsDeliveryByName);
                } else if(params.getWithdrawal()) {
                    List<JsonObject> resultsWithdrawalByName = new ArrayList<>();
                    for (int i = 0; i < jsonArrayWithdrawal.size(); i++) {
                        JsonObject item = jsonArrayWithdrawal.get(i).getAsJsonObject();
                        if(results.stream().filter(commerce -> commerce.equals(item)).count() > 0) {
                            item.addProperty("distance", this.distance(lat1,lng1,item.get("lat").getAsDouble(),item.get("lng").getAsDouble()));
                            if(item.get("hasWithdrawal").getAsBoolean()) {
                                item.addProperty("hasDelivery", false);
                                resultsWithdrawalByName.add(item);
                            }
                        };
                    }
                    results.clear();
                    results.addAll(resultsWithdrawalByName);
                } else {
                    List<JsonObject> resultsWithdrawalByName = new ArrayList<>();
                    List<JsonObject> resultsDeliveryByName = new ArrayList<>();

                    for (int i = 0; i < jsonArrayWithdrawal.size(); i++) {
                        JsonObject item = jsonArrayWithdrawal.get(i).getAsJsonObject();
                        if(results.stream().filter(commerce -> commerce.equals(item)).count() > 0) {
                            item.addProperty("distance", this.distance(lat1,lng1,item.get("lat").getAsDouble(),item.get("lng").getAsDouble()));
                            if(item.get("hasWithdrawal").getAsBoolean()) {
                                item.addProperty("hasDelivery", false);
                                resultsWithdrawalByName.add(item);
                            }
                        };
                    }

                    for (int i = 0; i < jsonArrayDelivery.size(); i++) {
                        JsonObject item = jsonArrayDelivery.get(i).getAsJsonObject();
                        if(results.stream().filter(commerce -> commerce.equals(item)).count() > 0) {
                            item.addProperty("distance", this.distance(lat1,lng1,item.get("lat").getAsDouble(),item.get("lng").getAsDouble()));
                            if(item.get("hasDelivery").getAsBoolean()) {
                                item.addProperty("hasWithdrawal", false);
                                resultsDeliveryByName.add(item);
                            }
                        }
                    }

                    results.clear();

                    for (JsonObject jsonObject : resultsWithdrawalByName) {
                        JsonObject item = jsonObject.getAsJsonObject();
                        if (resultsDeliveryByName.stream().filter(commerce -> commerce.get("id").equals(item.get("id"))).count() > 0) {
                            item.addProperty("hasDelivery", true);
                        }
                        results.add(item);
                    }

                    for (JsonObject jsonObject : resultsDeliveryByName) {
                        JsonObject item = jsonObject.getAsJsonObject();
                        if (resultsWithdrawalByName.stream().filter(commerce -> commerce.get("id").equals(item.get("id"))).count() == 0) {
                            results.add(item);
                        }
                    }
                }
            } else if(params.getDelivery()) {
                for (int i = 0; i < jsonArrayDelivery.size(); i++) {
                    JsonObject item = jsonArrayDelivery.get(i).getAsJsonObject();
                    item.addProperty("distance", this.distance(lat1,lng1,item.get("lat").getAsDouble(),item.get("lng").getAsDouble()));
                    if(item.get("hasDelivery").getAsBoolean()) {
                        item.addProperty("hasWithdrawal", false);
                        results.add(item);
                    }
                }
            } else if(params.getWithdrawal()) {
                for (int i = 0; i < jsonArrayWithdrawal.size(); i++) {
                    JsonObject item = jsonArrayWithdrawal.get(i).getAsJsonObject();
                    item.addProperty("distance", this.distance(lat1,lng1,item.get("lat").getAsDouble(),item.get("lng").getAsDouble()));
                    if(item.get("hasWithdrawal").getAsBoolean()) {
                        item.addProperty("hasDelivery", false);
                        results.add(item);
                    }
                }
            } else {
                List<JsonObject> resultsWithdrawal = new ArrayList<>();
                for (int i = 0; i < jsonArrayWithdrawal.size(); i++) {
                    JsonObject item = jsonArrayWithdrawal.get(i).getAsJsonObject();
                    item.addProperty("distance", this.distance(lat1,lng1,item.get("lat").getAsDouble(),item.get("lng").getAsDouble()));
                    if(item.get("hasWithdrawal").getAsBoolean()) {
                        item.addProperty("hasDelivery", false);
                        resultsWithdrawal.add(item);
                    }
                }

                List<JsonObject> resultsDelivery = new ArrayList<>();
                for (int i = 0; i < jsonArrayDelivery.size(); i++) {
                    JsonObject item = jsonArrayDelivery.get(i).getAsJsonObject();
                    item.addProperty("distance", this.distance(lat1,lng1,item.get("lat").getAsDouble(),item.get("lng").getAsDouble()));
                    if(item.get("hasDelivery").getAsBoolean()) {
                        resultsDelivery.add(item);
                    }
                }

                for (JsonObject jsonObject : resultsWithdrawal) {
                    JsonObject item = jsonObject.getAsJsonObject();
                    if (resultsDelivery.stream().filter(commerce -> commerce.get("id").equals(item.get("id"))).count() > 0) {
                        item.addProperty("hasDelivery", true);
                    }
                    results.add(item);
                }

                for (JsonObject jsonObject : resultsDelivery) {
                    JsonObject item = jsonObject.getAsJsonObject();
                    if (resultsWithdrawal.stream().filter(commerce -> commerce.get("id").equals(item.get("id"))).count() == 0) {
                        results.add(item);
                    }
                }
            }
            return ResponseEntity.ok(results.toString());
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    public ResponseEntity<?> show(String code) throws Exception {
        try {
            URI uri = new URI(String.format("%s/ecommerce/%s", this.uri, code));
            return this.api(uri, HttpMethod.GET);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    public ResponseEntity<?> showByName(String name) throws Exception {
        try {
            URI uri = new URI(String.format("%s/ecommerce/name/%s", this.uri, name));
            return this.api(uri, HttpMethod.GET);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    public ResponseEntity<?> category(String category) throws Exception {
        try {
            URI uri = new URI(String.format("%s/commerces/ecommerces/%s", this.uri, category));
            return this.api(uri, HttpMethod.GET);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    public ResponseEntity<?> products(String code) throws Exception {
        try {
            URI uri = new URI(String.format("%s/product/red/ecommerce/%s", this.uri, code));
            return this.api(uri, HttpMethod.GET);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    public ResponseEntity<?> validateProducts(String commerce, Object products) throws Exception {
        try {
            URI uri = new URI(String.format("%s/commerces/validate/stock/%s", this.uri, commerce));
            return this.api(uri, HttpMethod.POST, products);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    public ResponseEntity<?> isOpen(String code) throws Exception {
        try {
            URI uri = new URI(String.format("%s/commerces/is-open/%s", this.uri, code));
            return this.api(uri, HttpMethod.GET);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
            return (dist);
        }
    }
}
