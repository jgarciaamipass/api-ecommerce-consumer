package com.amipass.api.ecommerce_consumer.services.mac;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MAC {

    @Value("${server.mac}")
    public String uri;

    public String tokenMAC;

    /**
     *
     * @param uri       uri services
     * @param method    set protocol http
     * @return          return object
     * @throws ResponseStatusException
     */
    public ResponseEntity<?> api(URI uri, HttpMethod method) throws Exception {
        try {
            RestTemplate restTemplate = new RestTemplate();
            Map<String, String> params = new HashMap<>();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setBearerAuth(this.tokenMAC);

            HttpEntity<Object> request = new HttpEntity<>(params, headers);
            return ResponseEntity.ok(Objects.requireNonNull(restTemplate.exchange(URLDecoder.decode(uri.toString(), "UTF-8"), method, request, String.class).getBody()));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    /**
     *
     * @param uri       uri services
     * @param method    set protocol http
     * @param params    params
     * @return          return object
     * @throws ResponseStatusException
     */
    public ResponseEntity<?> api(URI uri, HttpMethod method, Object params) throws ResponseStatusException {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setBearerAuth(this.tokenMAC);

            HttpEntity<Object> request = new HttpEntity<>(params, headers);
            return ResponseEntity.ok(Objects.requireNonNull(restTemplate.exchange(uri, method, request, String.class).getBody()));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }

    /**
     *
     * @param uri       uri services
     * @param method    set protocol http
     * @param params    params
     * @param headers   set custom header
     * @return          return object
     * @throws ResponseStatusException
     */
    public ResponseEntity<?> api(URI uri, HttpMethod method, Object params, HttpHeaders headers) throws ResponseStatusException {
        try {
            RestTemplate restTemplate = new RestTemplate();
            headers.setBearerAuth(this.tokenMAC);
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Object> request = new HttpEntity<>(params, headers);
            return ResponseEntity.ok(Objects.requireNonNull(restTemplate.exchange(uri, method, request, String.class).getBody()));
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(e.getStatus());
        }
    }
}
