package com.amipass.api.ecommerce_consumer.services.mac;

import com.amipass.api.ecommerce_consumer.model.ShoppingCarEntity;
import com.amipass.api.ecommerce_consumer.model.ShoppingCarItemEntity;
import com.amipass.api.ecommerce_consumer.model.UserAddressEntity;
import com.amipass.api.ecommerce_consumer.model.UsersEntity;
import com.amipass.api.ecommerce_consumer.model.dto.mac.*;
import com.amipass.api.ecommerce_consumer.model.dto.mailer.EmailSenderDTO;
import com.amipass.api.ecommerce_consumer.repository.ShoppinCarRepository;
import com.amipass.api.ecommerce_consumer.repository.UserAddressRepository;
import com.amipass.api.ecommerce_consumer.repository.UsersRepository;
import com.amipass.api.ecommerce_consumer.services.EmailService;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class OrderService extends MAC {

    @Autowired
    private ShoppinCarRepository shoppinCarRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UserAddressRepository userAddressRepository;

    @Autowired
    private CommerceService commerceService;

    @Autowired
    private ProductService productService;

    @Autowired
    private EmailService emailService;

    public ResponseEntity<?> store(int id) throws ResponseStatusException {
        try {
            ShoppingCarEntity shoppingCarEntity = shoppinCarRepository.findById(id)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, ""));

            OrderHeaderDTO orderHeaderDTO = new OrderHeaderDTO();
            Collection<OrderDetailsDTO> orderDetailsDTO = new ArrayList<>();
            Collection<OrderPaymentsDTO> orderPaymentsDTO = new ArrayList<>();
            OrderConsumerDTO orderConsumerDTO = new OrderConsumerDTO();

            orderHeaderDTO.setCommerceID(shoppingCarEntity.getCommerce());
            orderHeaderDTO.setStoreID(shoppingCarEntity.getStore());
            orderHeaderDTO.setInternalCommerceID( (shoppingCarEntity.getCommerce()) + shoppingCarEntity.getStore());
            orderHeaderDTO.setTransactionID(String.format("e%s-%s", shoppingCarEntity.getId(), shoppingCarEntity.getTryPay()));
            orderHeaderDTO.setPosID(1);
            orderHeaderDTO.setChannelID(1);
            orderHeaderDTO.setStatusID(1);
            orderHeaderDTO.setOrderTypeID((shoppingCarEntity.getDelivery()) ? 2 : 1);
            orderHeaderDTO.setSellerID(1);

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();
            df.setTimeZone(TimeZone.getTimeZone("America/Santiago"));

            orderHeaderDTO.setCreationDate(df.format(now));
            System.out.println(df.format(now));

            orderHeaderDTO.setTotalAmount(shoppingCarEntity.getTotalAmount());
            orderHeaderDTO.setDiscountAmount(shoppingCarEntity.getDiscountAmount());
            orderHeaderDTO.setDeliveryAmount(shoppingCarEntity.getDeliveryAmount());
            orderHeaderDTO.setFinalAmount(shoppingCarEntity.getDeliveryAmount() + shoppingCarEntity.getTotalAmount());
            orderHeaderDTO.setWasPayByPromotion(false);

            int countItems = 0;
            int countQuantity = 0;
            for (ShoppingCarItemEntity item :
                    shoppingCarEntity.getShoppingCarItemsByShoppingCarId()) {
                OrderDetailsDTO detail = new OrderDetailsDTO();
                detail.setProductID(item.getProductIdMac());
                detail.setUnitPrice(item.getUnitPrice());
                detail.setTotalItems(item.getQuantity());
                detail.setFinalAmount(item.getTotal());
                countItems++;
                countQuantity +=item.getQuantity();
                orderDetailsDTO.add(detail);
            }
            orderHeaderDTO.setTotalItems(countItems);
            orderHeaderDTO.setTotalLines(countQuantity);

            OrderPaymentsDTO payment = new OrderPaymentsDTO();
            payment.setAmount(shoppingCarEntity.getDeliveryAmount() + shoppingCarEntity.getTotalAmount());
            payment.setCreditCardLastDigit(0);
            payment.setPaymentTypeID(shoppingCarEntity.getPaymentTypeId());
            payment.setQuotas(1);
            orderPaymentsDTO.add(payment);

            UsersEntity usersEntity = usersRepository.findById(shoppingCarEntity.getUserId())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado"));

            orderConsumerDTO.setUniqueID(usersEntity.getNumDocument());
            orderConsumerDTO.setAmipassInternalID(usersEntity.getId());
            orderConsumerDTO.setFirstName(shoppingCarEntity.getFullName());
            orderConsumerDTO.setLastName("");

            if(shoppingCarEntity.getDelivery()) {
                UserAddressEntity userAddressEntity = userAddressRepository.findById(shoppingCarEntity.getUserAddressId()).get();
                orderConsumerDTO.setAddress(String.format("%s (%s)", userAddressEntity.getAddress(), userAddressEntity.getPlace()));
                orderConsumerDTO.setCommune(userAddressEntity.getLocality());
            }

            orderConsumerDTO.setAddressComment("");
            orderConsumerDTO.setEmail(usersEntity.getEmail());
            orderConsumerDTO.setFmcToken("");
            orderConsumerDTO.setMobileNumber(shoppingCarEntity.getPhone());

            OrderDTO orderDTO = new OrderDTO();
            orderDTO.setOrderHeader(orderHeaderDTO);
            orderDTO.setOrderDetails(orderDetailsDTO);
            orderDTO.setOrderPayments(orderPaymentsDTO);
            orderDTO.setOrderConsumer(orderConsumerDTO);

            Map<String, Object> params = new HashMap<>();

            URI uri = new URI(String.format("%s/order", this.uri));

            params.put("orderHeader", orderDTO.getOrderHeader());
            params.put("orderDetails", orderDTO.getOrderDetails());
            params.put("orderPayments", orderDTO.getOrderPayments());
            params.put("consumer", orderDTO.getOrderConsumer());

//            return ResponseEntity.ok(params);

            JsonElement jsonElement = JsonParser.parseString(this.api(uri, HttpMethod.POST, params).getBody().toString());
            JsonObject jsonObject = jsonElement.getAsJsonObject();

            int idOrder = jsonObject.get("id").getAsInt();

            shoppingCarEntity.setOrderIdMac(idOrder);
            shoppingCarEntity.setStatus(2);
            shoppinCarRepository.save(shoppingCarEntity);

            //mail compra realizada

            EmailSenderDTO emailSenderDTO = new EmailSenderDTO();
            emailSenderDTO.setEmailTo(usersEntity.getEmail());
            String template = "/shopping/invoice.ftl";

            Map<String, Object> model = new HashMap<>();
            model.put("fullname", String.format("%s %s", usersEntity.getFirstName(), usersEntity.getLastName()));
            model.put("numOrder", idOrder);
            model.put("date", orderHeaderDTO.getCreationDate());
            String delivery = "";
            if(shoppingCarEntity.getDelivery()) {
                delivery = String.format("Despacho a la dirección: %s", orderConsumerDTO.getAddress());
            } else {
                delivery = "Retiro en local";
            }
            String paymentType = "";
            if(shoppingCarEntity.getPaymentTypeId() == 1) {
                paymentType = "Amipass";
            } else {
                paymentType = "Transbank";
            }
            model.put("delivery", delivery);
            model.put("payMethod", paymentType);
            model.put("products", shoppingCarEntity.getShoppingCarItemsByShoppingCarId());
            model.put("amount", shoppingCarEntity.getDeliveryAmount() + shoppingCarEntity.getTotalAmount());
            model.put("email", usersEntity.getEmail());
            emailSenderDTO.setSubject(String.format("Compra n° %s realizada", idOrder));
            emailService.sendMail(emailSenderDTO, template, model);

            return ResponseEntity.ok(jsonElement.toString());
        } catch (Exception e) {
//            System.out.println();
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> changeStatusOrder(Integer id, OrderStatusDTO orderStatusDTO)
            throws Exception {
        Map<String, Object> params = new HashMap<>();

        URI uri = new URI(String.format("%s/order/%s", this.uri, id.toString()));

        //Validate Status
//        JsonObject jsonOrder = JsonParser.parseString(Objects.requireNonNull(restTemplate.getForObject(uri, String.class))).getAsJsonObject();
        JsonObject jsonOrder = JsonParser.parseString(this.api(uri, HttpMethod.GET).getBody().toString()).getAsJsonObject();
        JsonObject jsonHeader = jsonOrder.get("orderHeader").getAsJsonObject();
        JsonObject jsonConsumer = jsonOrder.get("consumer").getAsJsonObject();

        int statusID = jsonHeader.get("statusID").getAsInt();
        int delivery = jsonHeader.get("orderTypeID").getAsInt();
        int amipassInternalID = jsonConsumer.get("amipassInternalID").getAsInt();
        String internalCommerceID = jsonHeader.get("internalCommerceID").getAsString();

        UsersEntity usersEntity = usersRepository.findById(amipassInternalID)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado"));

        JsonObject jsonCommerce = JsonParser.parseString(Objects.requireNonNull(commerceService.show(internalCommerceID).getBody().toString())).getAsJsonObject().get("commerce").getAsJsonObject();

        if(statusID > 1 && (orderStatusDTO.getStatus() == -2 | orderStatusDTO.getStatus() == -1)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La orden ya fue tomada por el comercio");
        }

        //1- Nueva
        //2- Confirmado
        //3- En camino
        //4- Entregado
        //-1- Cancelado por comercio
        //-2- Cancelado por usuario
        EmailSenderDTO emailSenderDTO = new EmailSenderDTO();
        emailSenderDTO.setEmailTo(usersEntity.getEmail());
        String template = null;

        Map<String, Object> model = new HashMap<>();
        model.put("email", usersEntity.getEmail());
        model.put("fullname", String.format("%s %s", usersEntity.getFirstName(), usersEntity.getLastName()));
        model.put("commerce", jsonCommerce.get("storeName").getAsString());
        model.put("numOrder", id.toString());

        System.out.println(orderStatusDTO.getStatus());

        if (orderStatusDTO.getStatus() >= statusID) {
            switch (orderStatusDTO.getStatus()) {
//                case 1:
//                    emailSenderDTO.setSubject("Compra realizada");
//                    template = "/shopping/confirmed.ftl";
//                    break;
                case 2:
                    emailSenderDTO.setSubject(String.format("Compra n° %s confirmada", id));
                    template = "/shopping/confirmed.ftl";
                    break;
                case 3:
                    if(delivery == 2) {
                        emailSenderDTO.setSubject(String.format("Orden n° %s en camino", id));
                        template = "/shopping/onWay.ftl";
                    } else {
                        emailSenderDTO.setSubject(String.format("Orden n° %s lista para retiro", id));
                        template = "/shopping/readyFromWithdrawal.ftl";
                    }
                    break;
                case 4:
                    emailSenderDTO.setSubject(String.format("Orden n° %s entregada", id));
                    template = "/shopping/delivered.ftl";
                    break;
                default:
                    break;
            }
            if(template != null && orderStatusDTO.getStatus() > 1) {
                System.out.println("Enviando mail");
                emailSenderDTO.setEmailTo(usersEntity.getEmail());
                emailService.sendMail(emailSenderDTO, template, model);
            }
        } else if(orderStatusDTO.getStatus() < 0) {
            switch (orderStatusDTO.getStatus()) {
                case -1:
                    emailSenderDTO.setSubject(String.format("Compra n° %s cancelada por el comercio", id));
                    template = "/shopping/commerceCanceled.ftl";
                    model.put("reason", orderStatusDTO.getReason());
                    break;
                case -2:
                    emailSenderDTO.setSubject(String.format("Compra n° %s cancelada", id));
                    template = "/shopping/canceled.ftl";
                    break;
                default:
                    break;
            }

            params.put("statusID", orderStatusDTO.getStatus());

            uri = new URI(String.format("%s/order/red/ecommerce/%s", this.uri, id));
//            ResponseEntity<?> response = restTemplate.exchange(uri, HttpMethod.PUT, request, String.class);
            ResponseEntity<?> response = this.api(uri, HttpMethod.PUT, params);
            if(response.getStatusCode() == HttpStatus.OK) {
                System.out.println("Enviando mail de cancelación");
                emailSenderDTO.setEmailTo(usersEntity.getEmail());
                emailService.sendMail(emailSenderDTO, template, model);
            }
        }
        return ResponseEntity.ok(HttpStatus.OK);
    }

    public ResponseEntity<?> show(Integer id) throws Exception {

        URI uri = new URI(String.format("%s/order/%s", this.uri, id.toString()));
        JsonObject jsonOrder = JsonParser.parseString(this.api(uri, HttpMethod.GET).getBody().toString()).getAsJsonObject();

        JsonObject jsonHeader = jsonOrder.get("orderHeader").getAsJsonObject();
        JsonArray jsonDetails = jsonOrder.get("orderDetails").getAsJsonArray();

        JsonObject commerce = JsonParser.parseString(Objects.requireNonNull(commerceService.show(jsonHeader.get("internalCommerceID").getAsString()).getBody().toString())).getAsJsonObject();
        jsonHeader.addProperty("commerce", commerce.get("commerce").getAsJsonObject().get("storeName").getAsString());

        for (int i = 0; i < jsonDetails.size(); i++) {
            Object products = productService.show(
                    jsonDetails.get(i).getAsJsonObject().get("productID").getAsInt(),
                    jsonHeader.get("internalCommerceID").getAsInt()
            ).getBody();
            JsonObject product = JsonParser.parseString(
                    Objects.requireNonNull(products.toString())
            ).getAsJsonObject();
            jsonDetails.get(i).getAsJsonObject().addProperty("name", product.get("product").getAsJsonObject().get("productName").getAsString());
            jsonDetails.get(i).getAsJsonObject().addProperty("description", product.get("product").getAsJsonObject().get("fullDescription").getAsString());
        }

        ShoppingCarEntity shoppingCarEntity = shoppinCarRepository.findByOrderIdMac(id);

        jsonOrder.addProperty("urlInvoice", shoppingCarEntity.getUrlInvoice());

        return ResponseEntity.ok(jsonOrder.toString());
    }
}
