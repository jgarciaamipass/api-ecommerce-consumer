package com.amipass.api.ecommerce_consumer.services.mac;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Service
public class ProductService {

    @Value("${server.mac}")
    private String uri;

    public ResponseEntity<?> show(Integer product, Integer commerce) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = new URI(String.format("%s/product/%s/%s", this.uri, product, commerce));
        return restTemplate.getForEntity(uri, String.class);
    }
}
