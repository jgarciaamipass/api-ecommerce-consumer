package com.amipass.api.ecommerce_consumer.services.users;

import com.amipass.api.ecommerce_consumer.model.dto.users.UniversalUserDTO;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class UniversalUserService {

    @Value("${server.universaluser}")
    private String uri;

    public ResponseEntity<?> login(UniversalUserDTO userDTO) throws Exception {
        try {
            String uriLogin = String.format("%s/login/account", uri);
            String uriUserExist = String.format("%s/account/userExist?username=%s", uri, userDTO.getUsername());
            int appId = 3;

            Map<String, Object> paramsLogin = new HashMap<>();
            paramsLogin.put("username", userDTO.getUsername());
            paramsLogin.put("password", userDTO.getPassword());
            paramsLogin.put("appId", appId);
            RestTemplate restTemplate = new RestTemplate();

            JsonObject jsonUserExist = JsonParser.parseString(Objects.requireNonNull(restTemplate.getForObject(uriUserExist, String.class))).getAsJsonObject();

            if(!jsonUserExist.get("exist").getAsBoolean()) {
                String uriCreateAccount = String.format("%s/account/createAccount", uri);
                Map<String, Object> paramsCreateAccount = new HashMap<>();
                paramsCreateAccount.put("username", userDTO.getUsername());
                paramsCreateAccount.put("password", userDTO.getPassword());
                paramsCreateAccount.put("appId", appId);
                paramsCreateAccount.put("provider", "INTERNAL");
                JsonObject jsonNewAccount = JsonParser.parseString(Objects.requireNonNull(restTemplate.postForObject(uriCreateAccount, paramsCreateAccount, String.class))).getAsJsonObject();
                System.out.println(jsonNewAccount.get("message").getAsString());
            }

            JsonObject jsonLogin = null;
            try {
                jsonLogin = JsonParser.parseString(Objects.requireNonNull(restTemplate.postForObject(uriLogin, paramsLogin, String.class))).getAsJsonObject();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return ResponseEntity.ok((jsonLogin != null) ? jsonLogin.get("token").getAsString() : null);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
