<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
  <table class="main-body c2513 c3008 c5534 c2332 c9129" width="100%" height="100%" bgcolor="#ffffff" style="box-sizing: border-box; min-height: 150px; padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px; width: 100%; height: 100%; background-color: #ffffff;">
    <tr class="row" id="c2521" valign="top" style="box-sizing: border-box; vertical-align: top;">
      <td class="main-body-cell" id="c3020" bgcolor="#ffffff" style="box-sizing: border-box; font-family: Helvetica, serif; font-weight: 700; font-size: 20px; background-color: #ffffff; border: 0px solid #e0e0e0;">
        <table class="c4258" id="c2529" width="100%" height="100%" align="center" style="box-sizing: border-box; height: 100%; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; max-width: 800px; border-radius: 0px 0px 0px 0px; border-collapse: separate; border: 0px solid #eeeeee; text-align: center;">
          <tr id="c2537" style="box-sizing: border-box;">
            <td class="c4270" id="c2541" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0;">
              <a id="c2369" href="#" style="box-sizing: border-box;"><img class="c4419" id="c3040" src="https://www.amipass.com/files/email/amimarket/bienvenida-no-usuario-amipass/img-01.jpg" style="box-sizing: border-box; color: black; width: 100%;"></a>
              <table class="c4510" id="c2548" width="100%" height="150" style="box-sizing: border-box; height: 150px; margin: 50px auto 10px auto; padding: 5px 5px 5px 5px; width: 100%;">
                <tr id="c2556" style="box-sizing: border-box;">
                  <td class="c4522" id="c2560" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0;">
                    <div class="c4599" id="c3059" style="text-align: left; box-sizing: border-box; padding: 10px 7% 25px 7%; font-weight: 500; font-size: 14px; color: #6f777d; width: 100%; border: 0px solid #000000;">Estimado(a) ${fullname}
                    </div>
                    <div class="c5374" id="c2568" style="text-align: left; font-weight: 500; margin: 0px 0px 25px 0px; box-sizing: border-box; padding: 10px 7% 0px 7%; font-size: 16px; color: #777777;">
                      <p class="MsoNormal" id="c9193" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                      <p class="MsoNormal" id="c9197" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">Bienvenido a
                        amiMARKET ¡Te estábamos esperando!
                      </p>
                      <p class="MsoNormal" id="c9201" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">Para ingresar a
                        tu cuenta, utiliza los siguientes datos:
                      </p>
                      <p class="MsoNormal" id="c9205" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <b id="c9209" data-highlightable="1" style="box-sizing: border-box;"> 
                        </b>
                      </p>
                      <p class="MsoNormal" id="c9213" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <b id="c9217" data-highlightable="1" style="box-sizing: border-box;">Usuario:
                        </b>
                        <span id="c7047" data-highlightable="1" lang="ES" style="box-sizing: border-box; mso-ansi-language: ES;"> ${email}</span>
                      </p>
                      <p class="MsoNormal" id="c9225" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <b id="c9229" data-highlightable="1" style="box-sizing: border-box;">Contraseña:
                        </b>
                        <span id="c7059" data-highlightable="1" lang="ES" style="box-sizing: border-box; mso-ansi-language: ES;"> Contraseña que ingresaste al crear la cuenta
                          de amiMARKET</span>
                      </p>
                      <p class="MsoNormal" id="c9237" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;"> 
                      </p>
                      <p class="MsoNormal" id="c9241" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <span id="c7071" data-highlightable="1" lang="ES" style="box-sizing: border-box; mso-ansi-language: ES;">Aquí encontrarás
                          los productos </span>
                        <span class="MsoCommentReference" id="c9249" data-highlightable="1" style="box-sizing: border-box;"><span id="c7079" data-highlightable="1" lang="ES-CL" style="box-sizing: border-box; font-size: 8.0pt; line-height: 107%;"><a class="msocomanchor" id="_anchor_1" href="#_msocom_1" name="_msoanchor_1" data-highlightable="1" style="box-sizing: border-box;"></a><span id="c7088" data-highlightable="1" style="box-sizing: border-box; mso-special-character: comment;"> </span></span></span>
                        <span id="c7092" data-highlightable="1" lang="ES" style="box-sizing: border-box; mso-ansi-language: ES;">que necesitas, promociones, descuentos y
                          más, todo en un solo lugar.</span>
                      </p>                      
                      <p class="MsoNormal" id="c9274" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <span id="c7104" data-highlightable="1" lang="ES" style="box-sizing: border-box; mso-ansi-language: ES;">Si requieres
                          asistencia escríbenos a </span>
                        <span id="c9282" data-highlightable="1" lang="ES-CL" style="box-sizing: border-box;"><a id="c9286" href="mailto:hola@amipass.com" data-highlightable="1" style="box-sizing: border-box;">hola@amipass.com</a></span>
                        <span id="c7117" data-highlightable="1" lang="ES" style="box-sizing: border-box; mso-ansi-language: ES;"> o al <a href="https://api.whatsapp.com/send?phone=56939121272&text=Hola, me gustaría recibir más información de amiMARKET"> whatsapp +56 9 3912 1272</a> (solo
                          chat).</span>
                      </p>
                      <p class="MsoNormal" id="c9295" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <span id="c9299" data-highlightable="1" lang="ES-CL" style="box-sizing: border-box;"></span>
                      </p>
                      <p class="MsoNormal" id="c9303" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                    </div>
                    <table class="c8448" id="c9307" width="100%" align="center" style="box-sizing: border-box; height: auto; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; text-align: center;">
                      <tr id="c9315" style="box-sizing: border-box;">
                        <td class="c8460" id="c9319" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0;">
                          <a id="c2543" href="http://www.amimarket.com" style="box-sizing: border-box;"><img class="c8544" id="c9328" src="https://www.amipass.com/files/email/amimarket/bienvenida-no-usuario-amipass/boton-01.png" style="box-sizing: border-box; color: black;"></a>
                        </td>
                      </tr>
                    </table>
                    <div class="c5374" id="c2568" style="text-align: left; font-weight: 500; margin: 0px 0px 25px 0px; box-sizing: border-box; padding: 10px 50px 0px 50px; font-size: 16px; color: #777777;">
                      <p class="MsoNormal" id="c9335" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                      <p class="MsoNormal" id="c9339" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                      <p class="MsoNormal" id="c9343" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                      <p class="MsoNormal" id="c9347" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <span class="MsoCommentReference" id="c9351" data-highlightable="1" style="box-sizing: border-box;"><span id="c7580" data-highlightable="1" lang="ES-CL" style="box-sizing: border-box; font-size: 8.0pt; line-height: 107%;"><a class="msocomanchor" id="_anchor_1" href="#_msocom_1" name="_msoanchor_1" data-highlightable="1" style="box-sizing: border-box;"></a><span id="c7576" data-highlightable="1" style="box-sizing: border-box; mso-special-character: comment;"> </span></span></span>
                      </p>
                      <p class="MsoNormal" id="c9368" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <span id="c9372" data-highlightable="1" lang="ES-CL" style="box-sizing: border-box;"></span>
                      </p>
                      <p class="MsoNormal" id="c9376" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">Saludos,
                      </p>
                      <p class="MsoNormal" id="c9380" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">Equipo amiMARKET
                      </p>
                      <p class="MsoNormal" id="c9384" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                    </div>
                  </td>
                </tr>
              </table>
              <table class="c3702" id="c5610" width="100%" height="150" bgcolor="#fbfbfb" align="center" style="box-sizing: border-box; height: 150px; margin: 70px auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; text-align: center; background-color: #fbfbfb;">
                <tr id="c5618" style="box-sizing: border-box;">
                  <td class="c3714" id="c5622" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0;">
                    <div class="c3789" id="c5626" style="box-sizing: border-box; padding: 10px;">Si tienes un problema estamos para ayudarte. 
                      <a class="link" id="c5634" href="#" data-highlightable="1" style="box-sizing: border-box; color: #FF1E2F;">Contáctanos</a>.
                    </div>
                    <div class="c3789" id="c5643" style="box-sizing: border-box; padding: 10px;">Te enviamos este e-mail a ${email} porque elegiste recibir información.
                      <br id="c5651" data-highlightable="1" style="box-sizing: border-box;">Conoce cómo cuidamos tu Privacidad y visita los Términos y Condiciones de 
                      <a class="link" id="c5659" href="#" data-highlightable="1" style="box-sizing: border-box; color: #FF1E2F;">amiMARKET.</a>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>