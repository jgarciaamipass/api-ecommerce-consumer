<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
  <table class="main-body c2513 c3008 c5534 c2333 c5100 c5642" width="100%" height="100%" bgcolor="#ffffff" style="box-sizing: border-box; min-height: 150px; padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px; width: 100%; height: 100%; background-color: #ffffff;">
    <tr class="row" id="c2521" valign="top" style="box-sizing: border-box; vertical-align: top;">
      <td class="main-body-cell" id="c3020" bgcolor="#ffffff" align="center" style="color: #000000; box-sizing: border-box; font-family: Helvetica, serif; font-weight: 700; font-size: 20px; background-color: #ffffff; border: 0px solid #e0e0e0; text-align: center;">
        <table class="c4258" id="c2529" width="100%" height="100%" align="center" style="box-sizing: border-box; height: 100%; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; max-width: 800px; border-radius: 0px 0px 0px 0px; border-collapse: separate; border: 0px solid #eeeeee; text-align: center;">
          <tr id="c2537" style="box-sizing: border-box;">
            <td class="c4270" id="c2541" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0;">
              <a id="c2370" href="#" style="box-sizing: border-box;"></a>
              <table class="c2733" id="c5137" width="100%" height="50" style="box-sizing: border-box; height: 50px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; border: 0px solid #e0e0e0;">
                <tr id="c5145" style="box-sizing: border-box;">
                  <td class="c2745" id="c5149" valign="top" height="50" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; height: 50px; background-color: #F7F7F7;" bgcolor="#F7F7F7">
                    <img class="c2878" id="c5153" src="https://www.amipass.com/files/email/amimarket/img/top-bar.png" style="background-color: rgba(0,0,0,0); box-sizing: border-box; color: black;">
                  </td>
                </tr>
              </table>
              <table class="c3306" id="c5156" width="100%" align="left" style="box-sizing: border-box; height: auto; margin: 50px auto 10px auto; padding: 0px 5px 5px 7%; width: 100%; font-size: 16px; text-align: left;">
                <tr id="c5164" style="box-sizing: border-box;">
                  <td class="c3318" id="c5168" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0;">
                    <div class="c3377" id="c5172" style="box-sizing: border-box; padding: 10px; font-size: 18px; color: #6f777d;">
                      <span id="c4406" data-highlightable="1" lang="ES" style="box-sizing: border-box; font-size: 24px; line-height: 107%; font-family: Helvetica, serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: ES; mso-fareast-language: EN-US; mso-bidi-language: AR-SA; font-weight: 400;">Tu
                        orden ya fue  </span>
                      <span class="c4180" id="c5180" data-highlightable="1" lang="ES" style="box-sizing: border-box; font-size: 24px; line-height: 107%; font-family: Helvetica, serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: 'Times New Roman'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: ES; mso-fareast-language: EN-US; mso-bidi-language: AR-SA; font-weight: 700; color: #6f777d;">ENTREGADA</span>
                    </div>
                  </td>
                </tr>
              </table>
              <table class="c4510" id="c2548" width="100%" height="150" style="box-sizing: border-box; height: 150px; margin: 50px auto 10px auto; padding: 5px 5px 5px 5px; width: 100%;">
                <tr id="c2556" style="box-sizing: border-box;">
                  <td class="c4522" id="c2560" valign="top" align="left" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; text-align: left;">
                    <div class="c4599" id="c3059" style="box-sizing: border-box; padding: 10px 7% 25px 7%; font-weight: 500; font-size: 14px; color: #6f777d; width: 100%; border: 0px solid #000000;">Estimado(a) ${fullname}
                    </div>
                    <div class="c5374" id="c2568" style="text-align: left; font-weight: 600; margin: 0px 0px 25px 0px; box-sizing: border-box; padding: 10px 7% 0px 7%; font-size: 18px; color: #777777;">
                      <span id="c6422" lang="ES" style="box-sizing: border-box; mso-ansi-language: ES;">Tu orden n° ${numOrder}
                        en ${commerce}, ya fue entregada con
                        éxito.</span>
                      <p class="MsoNormal" style="box-sizing: border-box;">¡Ahora a
                        disfrutar de tu compra!
                      </p>
                    </div>
                    <div class="c5374" id="c2568" style="text-align: left; font-weight: 500; margin: 0px 0px 25px 0px; box-sizing: border-box; padding: 10px 50px 0px 50px; font-size: 16px; color: #777777;">
                      <p class="MsoNormal" id="c9335" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                      <p class="MsoNormal" id="c9339" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                      <p class="MsoNormal" id="c9343" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                      <p class="MsoNormal" id="c9347" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <span class="MsoCommentReference" id="c9351" data-highlightable="1" style="box-sizing: border-box;"><span id="c7580" data-highlightable="1" lang="ES-CL" style="box-sizing: border-box; font-size: 8.0pt; line-height: 107%;"><a class="msocomanchor" id="_anchor_1" href="#_msocom_1" name="_msoanchor_1" data-highlightable="1" style="box-sizing: border-box;"></a><span id="c7576" data-highlightable="1" style="box-sizing: border-box; mso-special-character: comment;"> </span></span></span>
                      </p>
                      <p class="MsoNormal" id="c9368" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                        <span id="c9372" data-highlightable="1" lang="ES-CL" style="box-sizing: border-box;"></span>
                      </p>
                      <p class="MsoNormal" id="c9376" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">Saludos,
                      </p>
                      <p class="MsoNormal" id="c9380" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">Equipo amiMARKET
                      </p>
                      <p class="MsoNormal" id="c9384" data-highlightable="1" style="margin: 0px 0px 0px 0px; padding: 0px 0px 10px 0px; box-sizing: border-box;">
                      </p>
                    </div>
                  </td>
                </tr>
              </table>
              <table class="c3702" id="c5610" width="100%" height="150" bgcolor="#fbfbfb" align="center" style="box-sizing: border-box; height: 150px; margin: 70px auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; text-align: center; background-color: #fbfbfb;">
                <tr id="c5618" style="box-sizing: border-box;">
                  <td class="c3714" id="c5622" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0;">
                    <div class="c3789" id="c5626" style="box-sizing: border-box; padding: 10px;">Si tienes un problema estamos para ayudarte. 
                      <a class="link" id="c5634" href="#" data-highlightable="1" style="box-sizing: border-box; color: #FF1E2F;">Contáctanos</a>.
                    </div>
                    <div class="c3789" id="c5643" style="box-sizing: border-box; padding: 10px;">Te enviamos este e-mail a ${email} porque elegiste recibir información.
                      <br id="c5651" data-highlightable="1" style="box-sizing: border-box;">Conoce cómo cuidamos tu Privacidad y visita los Términos y Condiciones de 
                      <a class="link" id="c5659" href="#" data-highlightable="1" style="box-sizing: border-box; color: #FF1E2F;">amiMARKET.</a>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>